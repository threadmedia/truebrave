<?php

return [
	/*
	|--------------------------------------------------------------------------
	| Test Mode
	| Режим тестирования
	|--------------------------------------------------------------------------
	|
	| After process test payment and confirm it by Yandex
	| Kassa, you must set this option value as false
	|
	| После проведения тестового платежа и подтверждения
	| этого платежа Яндекс Кассой, необходимо установить
	| значение этой опции в false
	|
	*/
	'test_mode' => env('YA_KASSA_TEST_MODE', true),

	/*
	|--------------------------------------------------------------------------
	| Yandex Money shop parameters
	| Параметры магазина Яндекс Деньги
	|--------------------------------------------------------------------------
	*/
	'shop_id' => env('YA_KASSA_SHOP_ID', 1),
	'secret' => env('YA_KASSA_SECRET', 1),

	/*
	|--------------------------------------------------------------------------
	| Shop Password
	| Секретное слово магазина (shoppassword)
	|--------------------------------------------------------------------------
	|
	| Secret word for generating md5-hash
	|
	| Секретное слово для формирования md5-хэша
	|
	| @see https://tech.yandex.com/money/doc/payment-solution/shop-config/parameters-docpage/
	|
	*/
	'shop_password' => env('YA_KASSA_SHOP_PASSWORD', '123'),

	/*
	|--------------------------------------------------------------------------
	| Payment types
	| Способы оплаты
	|--------------------------------------------------------------------------
	|
	| Payment types that will be given in payment form.
	| All available payment types you can find
	| in Yandex Kassa documentation
	|
	| Способы оплаты, которые будут предложены в форме
	| оплаты. Все доступные способы оплаты можно найти
	| в документации Яндекс Кассы
	|
	| @see https://tech.yandex.com/money/doc/payment-solution/reference/payment-type-codes-docpage/
	|
	*/
	'payment_types' => [
		'AC', 
	],
];
