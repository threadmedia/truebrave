<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user', function (Blueprint $table) {
			$table->increments('id');
			$table->string('role', 10)->default('user')->comment('user, admin, root');
			$table->string('email', 50)->unique();
			$table->string('password')->nullable();
			$table->string('phone', 16)->nullable();
			$table->string('auth_token')->nullable();
			$table->string('team_name')->nullable();
			$table->string('city')->nullable();
			$table->string('cap_fio')->nullable();
			$table->rememberToken();			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('user');
	}
}
