<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TariffsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tariff')->insert([
			['title' => 'Разовый 3', 'price' => 50.00, 'period' => null, 'request_count' => 3, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],
			['title' => 'Разовый 6', 'price' => 90.00, 'period' => null, 'request_count' => 6, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],
			['title' => 'Разовый 9', 'price' => 120.00, 'period' => null, 'request_count' => 9, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],

			['title' => 'Подписка на 1 месяц', 'price' => 300.00, 'period' => 1, 'request_count' => null, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],
			['title' => 'Подписка на 3 месяца', 'price' => 400.00, 'period' => 3, 'request_count' => null, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],
			['title' => 'Подписка на 6 месяцев', 'price' => 500.00, 'period' => 6, 'request_count' => null, 'is_active' => 1, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'),],
		]);
	}
}
