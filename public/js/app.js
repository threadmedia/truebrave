$(document).ready(function () {
	/**
	 * Check cookie availability
	 */
	function check_coockie() {
		console.log('check_coockie: ' + $.cookie('cookie', 'available'));
	}
	check_coockie();

	// Mobile-menu
	$('.header__hamburger-toggle').on('click', function () {
		$('.header__menu-slide').addClass('header__menu-slide--active');
		$('.header__nav').addClass('header__nav--active');
	});
	$('.menu-slide__btn-close').on('click', function () {
		$('.header__menu-slide').removeClass('header__menu-slide--active');
		$('.header__nav').removeClass('header__nav--active');
	});

	$('#mobile-menu-list a').click( function() {
		$('.header__menu-slide').removeClass('header__menu-slide--active');
		$('.header__nav').removeClass('header__nav--active');
	})

	$('[data-toggle="datepicker"]').datepicker({
		language: 'ru-RU',
		autoHide: true,
	});

	$('.datepicker').mask("00.00.0000", {placeholder: "__.__.____"});
});
