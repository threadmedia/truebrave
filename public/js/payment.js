$(document).ready(function () {
	/**
	 * Pay
	 */
	$('#pay').click( function(event) {
		event.preventDefault();
		var btn = $(this);
		btn.prop('disabled', true).addClass('btn-loading');

		var data = {
			"_token": $('meta[name="csrf-token"]').attr('content'),
		};

		$.post( routes.payment.pay, data, function (r) {
			$('#payment-form #error-block').removeClass('hidden').removeClass('alert-warning').addClass('alert-success');
			btn.remove();
			window.location.replace(r.confirmation_url);
		})
		.done(function () {
		})
		.fail(function (error) {
			console.log(error);
		})
		.always(function() {
			btn.prop('disabled', false).removeClass('btn-loading');
		});
		btn.prop('disabled', false).removeClass('btn-loading');

		/*
		axios.post(routes.payment.pay, data)
		.then(function (r) {
			$('#payment-form #error-block').removeClass('hidden').removeClass('alert-warning').addClass('alert-success');
			btn.remove();
			window.location.replace(r.data.confirmation_url);
		})
		.catch(function (error) {
			console.log(error);
		})
		.finally(function () {
			btn.prop('disabled', false).removeClass('btn-loading');
		});
		btn.prop('disabled', false).removeClass('btn-loading');
		*/
	});

	function fill_table (data) {
		is_payed = false;
		var content = '';
		if (typeof data.payment_list !== 'undefined' && data.payment_list.length > 0) {
			$('#pay-block').addClass('hidden');

			$.each(data.payment_list, function (index, value) {
				content += '<tr>';
				content += '<td>'+value.id+'</td>';
				content += '<td>'+value.amount+' руб.</td>';
				content += '<td><small>'+value.created_at+'</small></td>';

				var status = '';
				if (value.status == 'pending') status = 'обрабатывается';
				if (value.status == 'waiting_for_capture') status =  'ожидают списания';
				if (value.status == 'succeeded') {status =  'успешно оплачен'; is_payed = true; };
				if (value.status == 'canceled') status =  'отменен';

				content += '<td>'+status+'</td>';
				content += '</tr>';
			});

			if(is_payed !== true) {
				$('#pay-block').removeClass('hidden');
			}
			else {
				$('#pay-block').addClass('hidden');
				$('#unique-code').removeClass('hidden');
				if (data.unique_code !== 0) {
					$('#scrapycoco').html('<h3>'+ data.unique_code + '</h3>');
				}
			}
		}
		$('#payment-table-body').html(content);
	}


	function check_payment_status() {

		$.post( routes.payment.check, function (r) {
			console.log(r);
			fill_table(r);
			// fill_table(r.data);
		})
		.done(function () {
		})
		.fail(function (error) {
			console.log(error);
		})
		.always(function() {
		});

		/*
		axios.post(routes.payment.check)
		.then(function (r) {
			console.log(r);
			fill_table(r.data);
		})
		.catch(function (error) {
			console.log(error);
		})
		.finally(function () {
			// btn.prop('disabled', false).removeClass('btn-loading');
		});
		*/
	}

	check_payment_status();
	setInterval(check_payment_status, 7000);
});