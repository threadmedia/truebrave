$(document).ready(function () {

	// Smooth scroll
	$('.header__menu-list li a, .menu-slide__list li a').on('click', function () {
		var el = $(this);
		if (el.hasClass('order-link'))
		 return true;
		var dest = el.attr('href');
		if (dest !== undefined && dest !== '') {
			$('html, body').animate({
				scrollTop: $(dest).offset().top
			}, 700);
		}
		return false;
	});

	$(window).scroll(function () {
		if ($(this).scrollTop() >= 300) {
			$('.cover-filter').fadeIn(300);
		} else{
			$('.cover-filter').fadeOut(300);
		}
	});
});
