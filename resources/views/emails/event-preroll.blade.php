@php
	$logo_link = 'https://lh3.googleusercontent.com/Q153KSQAf7vO2CvCcBL5bB5h3v5m0zyHif2SOWADnhc8n-DE_JWSm84AkLe8hT3VvWdE5_374eBBuoVMW3UFnnhlY2wdCEkVtT7XdM651zM1eMBG0zmeNaH3jZBXBI9Cmxdual-YMJHnY4YFBznmZMfLKQnoc7cXfpf74cEtpF3PJ9ObQhHLAvdB6Ji417qsapwGmNknFustn48g94NnKZYmJldzKg1jkCZ6-Ml1BosNoddIknlrf4Fn0JME2nuZ-sJS5LRlRRZtoaxJHc47kecgByH4hDKVkoIt7FNCrZjw8BENZw8C_Wfqqurgj0aZ_-0dvNXI9RMkhCLZB2Yy7xCYDJJepl2RIWJrapywz20vSW_O2AfzAHRIloT2yW583gJhFRiFf0m2xF94Jd7dROrBHV71APrBWF_zRceZXAEDLSLuEVfm4GgV8YG0lK4ar0BcoK9fiV-57wlBjdBI4EbT2M5rdgcqv-sMneLvZ0yyz5-hXzBc7iiiSErGKNrFBrlqDLoSfOqsSofN0euPuBh-3vHa_pHND2GojiDMurrthNQEs3TVNFUG3h20-a6qeQfFKM10mM95v_cVzYdcylA_nVDXbDvmIvPoH1G3__VwrjhK8x3lZg6z-M9L3LzlnWsWrcWEVDP_VBK3dOWO-F6_xI7SHeM=w132-h74-no';
	
	$padding = 'padding: 20px 30px';
	$logo = [
		'max-width: 100%',
		'border: 0',
		'outline: none',
		'text-decoration:none',
		'display: block',
		'margin-left: auto',
		'margin-right: auto',
		'padding-top: 40px',
	];

	$btn = [
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 24px',
		'background-color: #B91F2F',
		'color: #FFF',
		'display: inline-block',
		'text-align: center',
		'text-decoration: none',
		'padding: 15px 30px',
	];

	$link = [
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 16px',
		'color: #B91F2F',
		'line-height: 19px',
	];

	$code_block = [
		'color: #ffffff',
		'font-size: 28px',
		'font-weight: bold',
		'display: block',
		'text-align: center',
	];

	$text_block = [
		'color: #ffffff',
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 16px',
		'line-height: 19px',
	];
@endphp


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="ru">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap&subset=cyrillic" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Russo+One&display=swap&subset=cyrillic" rel="stylesheet">
</head>

<body>

	<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" width="560px">
		<tbody>
			<tr>
				<td>
					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						<img alt="Дело Храбрых" width="132" height="74" border="0" style="{{ implode('; ', $logo) }};" src="{{$logo_link}}">
					</div>
				</td>
			</tr>

			<tr>
				<td>
					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						<h4>Добрый день, {{ $user->cap_fio}}!</h4>
						<p>Если вы получили это письмо, значит вы капитан одной из 96 команд экстремального забега «Дело храбрых», и её уже ждет 4-километровая трасса.</p>
						<p>Давайте по порядку.</p>
						
						<p>Запомните, сохраните или запишите этот уникальный регистрационный номер, он  будет нужен вам для получения стартовых пакетов:</p>
						
						<div class="text-center">
							<div style="{{ $padding }}; {{ implode('; ', $code_block) }};">
								{{ $user->code }}
							</div>
						</div>
						<p>Это ваша команда, ее название <strong>{{ $user->team_name }}</strong>,<br> ее стартовый номер <strong>{{ $user->team_number }}</strong></p>
						<p>А это ее члены:</p>
						<ul>
							@foreach($user->teamates as $teamate)
								<li>{{ $teamate->fio }}</li>
							@endforeach
						</ul>

						<p>ВАЖНО!<br> 
							Чтобы получить стартовые пакеты, капитану команды необходимо иметь при себе: оригинал паспорта или иной документ, подтверждающий его личность; копии паспортов или документов участников команды. Каждый член команды должен заполнить бланк отказа от ответственности, которые нужно будет отдать в штаб мероприятия (его можно найти на площадке по указателям)
							<br><br>
							<a href="{{ config('app.url') }}/docs/disclaimer.pdf" target="_blank" style="{{ implode('; ', $link) }};">[№ 1 БЛАНК ОСВОБОЖДЕНИЯ ОТ ОТВЕТСТВЕННОСТИ]</a>
						</p>
						
						 
						<p>По этой ссылке вы можете ознакомиться с положением забега, там есть вся необходимая и важная информация:<br><br> 
							<a href="{{ config('app.url') }}/docs/regulations.pdf" target="_blank" style="{{ implode('; ', $link) }};">[№ 2 ПОЛОЖЕНИЕ]</a>
						</p>

						<p>Это ссылка на бланк передачи участия, он понадобится в случае, если вдруг нужно заменить капитана команды. Документ также передается организаторам в штаб:<br><br> 
							<a href="{{ config('app.url') }}/docs/assignment.pdf" target="_blank" style="{{ implode('; ', $link) }};">[№ 3 БЛАНК ПЕРЕДАЧИ УЧАСТИЯ]</a>
						</p>

						<p>НЕ МЕНЕЕ ВАЖНО! Правила прохождения препятствий, ознакомьтесь с ними всей командой, чтобы пройти трассу без нарушений и побороться за денежный приз:<br><br> 
							<a href="{{ config('app.url') }}/docs/Rules.pdf" target="_blank" style="{{ implode('; ', $link) }};">[№ 4 ПРАВИЛА ЗАБЕГА]</a>
						</p>

						<p>А это карта забега, здесь есть все препятствия по порядку:<br><br> 
							<a href="{{ config('app.url') }}/docs/map.jpg" target="_blank" style="{{ implode('; ', $link) }};">[№ 5 КАРТА ЗАБЕГА]</a>
						</p>

						<p>Расписание на сегодня:</p>

						<p>
							10:00 – Начало выдачи стартовых пакетов в палатке штаба (в штаб за стартовым пакетом идет только капитан команды!); начало работы мандатной комиссии и раздевалок;
							<br>11:00 – Просмотр трассы для капитанов команд, которые претендуют на призовой фонд. Оглашение правил. Место сбора у палатки штаба, ее можно найти по указателям;
							<br>12:00 – Официальное открытие мероприятия;
							<br>13:00 – Старт 1 забега. Номера команд с 1 – 24;
							<br>13:30 – Старт 2 забега. Номера команд с 25 – 48;
							<br>14:00 – Старт 3 забега. Номера команд с 49 – 72;
							<br>14:30 – Старт 4 забега. Номера команд с 73 – 96;
							<br>16:00 – Торжественное подведение итогов, награждение победителей и призеров забега. Церемония закрытия.
						</p>

						<p>Ну и на последок содержимое стартового пакета:</p>
						<ul>
							<li>электронный чип хронометража для капитана команды(крепится на голень);</li>
							<li>брендированный силиконовый браслет - 5 шт;</li>
							<li>стартовый номер - 5 шт; (крепится на левое плечо при помощи резинок); </li>
							<li>мешок для сдачи вещей в камеру хранения – 1 шт;  </li>
							<li>наклейка с номером команды наклеиваемая на мешок – 1 шт;</li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" style="padding-top: 20px; padding-left: 38px; padding-right: 38px; border-top: 2px solid #B91F2F;" width=560px>
						<tbody>
							<tr>
								<td align="center">
									<a href="http://www.sportfond.team/" target="_blank">
										<img alt="Фонд поддержки и развития" src="https://lh3.googleusercontent.com/dBSiIAnH05Qj-YDyZPmE2UyMnFadgd5cqrFEqW5Xn_txrwvcOW4tpZL1GP4dqete2gIYMppAyp9Rf6S-_u1NQpYHlsJ6uj94NlYBfzrfbv5uFZMfoxxgGn4FjZh8r0adqwu480i4WHNM9Y6AeyYJlLHSpS1n578vqOyD-Z1XamxrEq_-eAHLtwDAc0D5oVfzkQV2iXag3GyLkseQJUUfVn8RCW5Utw9TmGclPajUz75k5euWUbTI83V4AJECEhmudBwzcSqEzOVA4kqBqOHDiQkjnqwX5s0VYN-GR4Czf4O73DWy7RD298J-zuXsiIiJjwgYQ8zqFAKz4grjPCw0a6tW1qITH3CnVXat5ruwXsRCyPVsIeloYqFMN0zjMXBeYeyzKbOqJtuCPYN_pvoboZeZl5uTUbmnXHu-Z240A2rncR2Kvb2lqUiCHJlXRob1iYe4b62EVNvxPrUSbkahsd8KdgI2yFAJrXUUtyp4-MHGIryyMyP7v6R_YtB7EoMudLPZFvQx6xjOrwDOtpwsThPhN1Z3AXf-E-jt7uW-arTUCWUQ4o1qYzsVPbt_pAcXtiNcXxuqoEnW6KMW4cQbEOGuPhk_bLpsE2mDLT_WbYQPKAYPl8m0czLaJTxYOit5VbzzS311ptK4q14XkMpAspdgW5z6ChQ=w259-h66-no">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" style="padding-top: 20px; padding-left: 38px; padding-right: 38px; padding-bottom: 40px;" width=560px>
						<tbody>
							<tr>
								<td align="center">
									<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" width=120px>
										<tbody>
											<tr>
												<td>
													<a href="https://vk.com/truebrave" target="_blank">
														<img alt="Вконтакте" src="https://lh3.googleusercontent.com/Pn9oKiyC5Wz0gxps5yhIhTTggUADRSVsEmK1gRtNPc_LxcAZWu4RAexrmxT9LfzozDQ2Jj9JICjKaCA7S81FKp7r4Lq0AyXSIIGs664_WDAqQKnxS1hx7lz6wJxzI5kG4xkmzAVrRF9CjNldEcCFEJOOcIKWIh7KgPPe1xwZeev-HOOkjk8Q0CuHiPl_fWfkodP4K6kYQCxNfYGWXQC6lTILU8ID2Rtbwl6wcGPd4Mof8kMohv_-QmcP6cp95NP97_a8AXxMjPevzsUG7ATh2VM9v6z2b41dbUmf_lSojK02OslZFisZsRYWf-o6zeV63cu3I4g55VCXVZmVfo1EhzY2vtdz9kgbarMaD6XhpQ81AOmlq_0IXLN0LFYKSaOA6NHz04S49csOUuiPwPNIOJmoY20XDMobYW87tyITH0vhSeAl7WDg4DC3FO8BqmljUB2kT7OBjip_Ep0WbseOKQWvHsAZuEx_-RmxUiwCDZ_ShvqL2KVihGvCy4UOkaDbTJAWuEH15X5WVeYq-fcwvwIxsQHu32FM90CxY_E81xXwXbg2R7pZkDqECQ7KjI93NROPgQ4_fTZJIJDcmaOdaemIpJr8v8t_b1yVEjsraiOdX1Xc6FmtVW2aGywjrVjdWDPZE9-ak_Z4E_jnp9s3Lv6zl59ohGs=w34-h19-no">
													</a>
												</td>
												<td>
													<a href="https://www.facebook.com/sportfondhm/" target="_blank">
														<img alt="Фейсбук" src="https://lh3.googleusercontent.com/D4S8dY1DCRVjo6CIwS8iDspBkjSb0r-BFR4AoQIlG2trihhXIX7wjbJ5-kI1tv-gB-cDg1hzmj30T-3s0S5CFGC9Xqwgqco5gEqHO_zl3c6ywWVXpb7Z5LPT4N3ucCOfgNJoGazL0SXtyuj8B4QmE8LhrVALz5pUV83henDUofweJW3UNz6UOcYnF0iT6egkntPQBZU-9qrpDlhviv92lZtMa505YCaaj_oNNxVQkxJZ0EpG8HOSKs6KiqFcWdp8NnRw8iBmabH0QPx6vgLCPemz2eJ6Qgwi53njPafkJbqhI09RJ-vosfcWT7K8HkBB-p_yZon58Q4ZOOzGC3_nT0MAzewkE7_krQ8Dt3bMIwDwijbIMlO1ULIuxxXYcsOJlxDm-chfdiLyUgRTLVCnn5ci6vV8YeJeddfYVIJzfKxOz1kB0TsOh1t3ns04WCZfiacGR7FfyVpw67ZlThxBePYYEDIKO0L6-Oa4efhXyFoWMu5_w3VuvwAFuWxhkzc93wKs7TJl6EObFFL36gd0yMx-Qf0zp04voUbcPMwpko9lZiuwMrJuJkOzjbEFOU9oy6S2nzBHf180n0BUD87n8haNFgG5G9Q6xy19QCDQx2GdpvbJ_LOrWQvMv-IsCJ1sfQ8LBXPjtsw0c-xF1Ii-T7T0jdqOfw0=w13-h23-no">
													</a>
												</td>
												<td align="center">
													<a href="https://www.instagram.com/sportfondhm/" target="_blank">
														<img alt="Инстаграмм" src="https://lh3.googleusercontent.com/o5ad5pB46K2imL-A-wdK4nkL29mWXvCViXrESJbSi20vTMwYZ3SlL65VI_SVekdstgsItc_iog4VaPY18k8xVBRv4XCQFLT15uD8OLy7vJuNaR_JXpZOUp1V5Eer2vFCyTBSi44xNbOUgBZhWVvlQNvbfsCMLLDYbFUPJI5A2W9K0SaCnDSegUpk9ekT_4GinQqLVTqM2bQm4-t69gwisazW3XSnpxqE0WNkWJaGXSxLQG6p2lmJJb5vXUDb4yHqzHYnucV6BtmfR9lzE2_0EQZxOadLe2hW29pl97iCBg8fAd_-pK0346Y7XRu7sbHO4DX5aMvGVqUjnfFErIMgJnCvwXS5WOA9nbN3ZmTARsHVAJBQ_kZhsZBj3LoKXUGb-uiK7JdEMqfvM70WjalQga1XU0_F48XqyND0EL_XkyrpWDFZbWtPzR18bOgovLEngIL6zg421zxM4BTDNK43MS013tQgqDkaBYTddMpymTKCV34hhfI0SuHdMs4bgECu9Pfi6j8gU5WRJFm0fsWAOd-3lYD_iSChI8DFfy4xBpbBkgegXb0E8etRPemeMLXtowrdu5EHwaWn6ChgevyoOotn-9D6d3KZhW5DphBrZoGU8QGCNS-7BiODx_BPTpGaUyWUi7gxsvViI1Nrr9luBqMLxY4GqpE=w22-h23-no">
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>