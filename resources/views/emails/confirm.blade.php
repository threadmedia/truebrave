@php
	$logo_link = 'https://lh3.googleusercontent.com/Q153KSQAf7vO2CvCcBL5bB5h3v5m0zyHif2SOWADnhc8n-DE_JWSm84AkLe8hT3VvWdE5_374eBBuoVMW3UFnnhlY2wdCEkVtT7XdM651zM1eMBG0zmeNaH3jZBXBI9Cmxdual-YMJHnY4YFBznmZMfLKQnoc7cXfpf74cEtpF3PJ9ObQhHLAvdB6Ji417qsapwGmNknFustn48g94NnKZYmJldzKg1jkCZ6-Ml1BosNoddIknlrf4Fn0JME2nuZ-sJS5LRlRRZtoaxJHc47kecgByH4hDKVkoIt7FNCrZjw8BENZw8C_Wfqqurgj0aZ_-0dvNXI9RMkhCLZB2Yy7xCYDJJepl2RIWJrapywz20vSW_O2AfzAHRIloT2yW583gJhFRiFf0m2xF94Jd7dROrBHV71APrBWF_zRceZXAEDLSLuEVfm4GgV8YG0lK4ar0BcoK9fiV-57wlBjdBI4EbT2M5rdgcqv-sMneLvZ0yyz5-hXzBc7iiiSErGKNrFBrlqDLoSfOqsSofN0euPuBh-3vHa_pHND2GojiDMurrthNQEs3TVNFUG3h20-a6qeQfFKM10mM95v_cVzYdcylA_nVDXbDvmIvPoH1G3__VwrjhK8x3lZg6z-M9L3LzlnWsWrcWEVDP_VBK3dOWO-F6_xI7SHeM=w132-h74-no';
	
	$padding = 'padding: 20px 30px';
	$logo = [
		'max-width: 100%',
		'border: 0',
		'outline: none',
		'text-decoration:none',
		'display: block',
		'margin-left: auto',
		'margin-right: auto',
		'padding-top: 40px',
	];

	$btn = [
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 24px',
		'background-color: #B91F2F',
		'color: #FFF',
		'display: inline-block',
		'text-align: center',
		'text-decoration: none',
		'padding: 15px 30px',
	];

	$link = [
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 16px',
		'color: #B91F2F',
		'line-height: 19px',
	];

	$text_block = [
		'color: #ffffff',
		'font-family: Roboto, Helvetica, Arial, serif !important',
		'font-size: 16px',
		'line-height: 19px',
	];
@endphp

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="ru">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap&subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="css/fonts.css">
</head>

<body>
	<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" width="560px">
		<tbody>
			<tr>
				<td>
					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						<img alt="Дело Храбрых" width="132" height="74" border="0" style="{{ implode('; ', $logo) }};" src="{{$logo_link}}">
					</div>
				</td>
			</tr>

			<tr>
				<td>
					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						Вы благополучно создали учетную запись!<br> Чтобы подтвердить ваш адрес электронной почты и продолжить регистрацию на мероприятие, щелкните по кнопке ниже
					</div>

					<div style="{{ $padding }}; text-align: center;">
						<a href="{{ config('app.url') }}/activate?token={{ $user->auth_token }}" target="_blank"  style="{{ implode(';', $btn) }};">ПОДТВЕРДИТЬ</a>
					</div>

					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						или скопируйте ссылку в адресную строку вашего браузера:
					</div>

					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						<a href="{{ config('app.url') }}/activate?token={{ $user->auth_token }}" style="{{ implode('; ', $link) }};">
							{{ config('app.url') }}/activate?token={{ $user->auth_token }}
						</a>
					</div>

					<div style="{{ $padding }}; {{ implode('; ', $text_block) }};">
						Если у вас возникли трудности с активацией учетной записи, обратитесь в Штаб мероприятия по телефону 8 800 3500 176
					</div>
				</td>
			</tr>

			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C"
						style="padding-top: 20px; padding-left: 38px; padding-right: 38px; border-top: 2px solid #B91F2F;" width=560px>
						<tbody>
							<tr>
								<td align="center">
									<a href="http://www.sportfond.team/" target="_blank">
										<img alt="Фонд поддержки и развития"
											src="https://lh3.googleusercontent.com/dBSiIAnH05Qj-YDyZPmE2UyMnFadgd5cqrFEqW5Xn_txrwvcOW4tpZL1GP4dqete2gIYMppAyp9Rf6S-_u1NQpYHlsJ6uj94NlYBfzrfbv5uFZMfoxxgGn4FjZh8r0adqwu480i4WHNM9Y6AeyYJlLHSpS1n578vqOyD-Z1XamxrEq_-eAHLtwDAc0D5oVfzkQV2iXag3GyLkseQJUUfVn8RCW5Utw9TmGclPajUz75k5euWUbTI83V4AJECEhmudBwzcSqEzOVA4kqBqOHDiQkjnqwX5s0VYN-GR4Czf4O73DWy7RD298J-zuXsiIiJjwgYQ8zqFAKz4grjPCw0a6tW1qITH3CnVXat5ruwXsRCyPVsIeloYqFMN0zjMXBeYeyzKbOqJtuCPYN_pvoboZeZl5uTUbmnXHu-Z240A2rncR2Kvb2lqUiCHJlXRob1iYe4b62EVNvxPrUSbkahsd8KdgI2yFAJrXUUtyp4-MHGIryyMyP7v6R_YtB7EoMudLPZFvQx6xjOrwDOtpwsThPhN1Z3AXf-E-jt7uW-arTUCWUQ4o1qYzsVPbt_pAcXtiNcXxuqoEnW6KMW4cQbEOGuPhk_bLpsE2mDLT_WbYQPKAYPl8m0czLaJTxYOit5VbzzS311ptK4q14XkMpAspdgW5z6ChQ=w259-h66-no">
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" style="padding-top: 20px; padding-left: 38px; padding-right: 38px; padding-bottom: 40px;" width=560px>
						<tbody>
							<tr>
								<td align="center">
									<table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#1C1C1C" width=120px>
										<tbody>
											<tr>
												<td>
													<a href="https://vk.com/truebrave" target="_blank">
														<img alt="Вконтакте" src="https://lh3.googleusercontent.com/Pn9oKiyC5Wz0gxps5yhIhTTggUADRSVsEmK1gRtNPc_LxcAZWu4RAexrmxT9LfzozDQ2Jj9JICjKaCA7S81FKp7r4Lq0AyXSIIGs664_WDAqQKnxS1hx7lz6wJxzI5kG4xkmzAVrRF9CjNldEcCFEJOOcIKWIh7KgPPe1xwZeev-HOOkjk8Q0CuHiPl_fWfkodP4K6kYQCxNfYGWXQC6lTILU8ID2Rtbwl6wcGPd4Mof8kMohv_-QmcP6cp95NP97_a8AXxMjPevzsUG7ATh2VM9v6z2b41dbUmf_lSojK02OslZFisZsRYWf-o6zeV63cu3I4g55VCXVZmVfo1EhzY2vtdz9kgbarMaD6XhpQ81AOmlq_0IXLN0LFYKSaOA6NHz04S49csOUuiPwPNIOJmoY20XDMobYW87tyITH0vhSeAl7WDg4DC3FO8BqmljUB2kT7OBjip_Ep0WbseOKQWvHsAZuEx_-RmxUiwCDZ_ShvqL2KVihGvCy4UOkaDbTJAWuEH15X5WVeYq-fcwvwIxsQHu32FM90CxY_E81xXwXbg2R7pZkDqECQ7KjI93NROPgQ4_fTZJIJDcmaOdaemIpJr8v8t_b1yVEjsraiOdX1Xc6FmtVW2aGywjrVjdWDPZE9-ak_Z4E_jnp9s3Lv6zl59ohGs=w34-h19-no">
													</a>
												</td>
												<td>
													<a href="https://www.facebook.com/sportfondhm/" target="_blank">
														<img alt="Фейсбук" src="https://lh3.googleusercontent.com/D4S8dY1DCRVjo6CIwS8iDspBkjSb0r-BFR4AoQIlG2trihhXIX7wjbJ5-kI1tv-gB-cDg1hzmj30T-3s0S5CFGC9Xqwgqco5gEqHO_zl3c6ywWVXpb7Z5LPT4N3ucCOfgNJoGazL0SXtyuj8B4QmE8LhrVALz5pUV83henDUofweJW3UNz6UOcYnF0iT6egkntPQBZU-9qrpDlhviv92lZtMa505YCaaj_oNNxVQkxJZ0EpG8HOSKs6KiqFcWdp8NnRw8iBmabH0QPx6vgLCPemz2eJ6Qgwi53njPafkJbqhI09RJ-vosfcWT7K8HkBB-p_yZon58Q4ZOOzGC3_nT0MAzewkE7_krQ8Dt3bMIwDwijbIMlO1ULIuxxXYcsOJlxDm-chfdiLyUgRTLVCnn5ci6vV8YeJeddfYVIJzfKxOz1kB0TsOh1t3ns04WCZfiacGR7FfyVpw67ZlThxBePYYEDIKO0L6-Oa4efhXyFoWMu5_w3VuvwAFuWxhkzc93wKs7TJl6EObFFL36gd0yMx-Qf0zp04voUbcPMwpko9lZiuwMrJuJkOzjbEFOU9oy6S2nzBHf180n0BUD87n8haNFgG5G9Q6xy19QCDQx2GdpvbJ_LOrWQvMv-IsCJ1sfQ8LBXPjtsw0c-xF1Ii-T7T0jdqOfw0=w13-h23-no">
													</a>
												</td>
												<td align="center">
													<a href="https://www.instagram.com/sportfondhm/" target="_blank">
														<img alt="Инстаграмм" src="https://lh3.googleusercontent.com/o5ad5pB46K2imL-A-wdK4nkL29mWXvCViXrESJbSi20vTMwYZ3SlL65VI_SVekdstgsItc_iog4VaPY18k8xVBRv4XCQFLT15uD8OLy7vJuNaR_JXpZOUp1V5Eer2vFCyTBSi44xNbOUgBZhWVvlQNvbfsCMLLDYbFUPJI5A2W9K0SaCnDSegUpk9ekT_4GinQqLVTqM2bQm4-t69gwisazW3XSnpxqE0WNkWJaGXSxLQG6p2lmJJb5vXUDb4yHqzHYnucV6BtmfR9lzE2_0EQZxOadLe2hW29pl97iCBg8fAd_-pK0346Y7XRu7sbHO4DX5aMvGVqUjnfFErIMgJnCvwXS5WOA9nbN3ZmTARsHVAJBQ_kZhsZBj3LoKXUGb-uiK7JdEMqfvM70WjalQga1XU0_F48XqyND0EL_XkyrpWDFZbWtPzR18bOgovLEngIL6zg421zxM4BTDNK43MS013tQgqDkaBYTddMpymTKCV34hhfI0SuHdMs4bgECu9Pfi6j8gU5WRJFm0fsWAOd-3lYD_iSChI8DFfy4xBpbBkgegXb0E8etRPemeMLXtowrdu5EHwaWn6ChgevyoOotn-9D6d3KZhW5DphBrZoGU8QGCNS-7BiODx_BPTpGaUyWUi7gxsvViI1Nrr9luBqMLxY4GqpE=w22-h23-no">
													</a>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

		</tbody>
	</table>
</body>
</html>