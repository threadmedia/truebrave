@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

				<div class="form-card">
					<div class="form-card__header">
						<h2>Вы успешно оплатили <br>пакет "На память"</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<h3 class="text-center">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</h3>
						<p>О месте и времени старта мероприятия вы будете извещены позднее. Мы уведомим Вас по электронной почте.</p> 
						<p>В случае если вы приобрели сувенирный пакет, в письме также будут содержаться инструкции по его получению.</p>

						@if(Auth::id())
							@php
								$user_package = Auth::user()->user_packages()->first();
								if(isset($user_package->id)) {
									$package_type = App\PackageType::find($user_package->package_type_id);
									$package = App\Package::find($user_package->package_id);
								}
								
							@endphp
						@endif

						<div>
							@if(isset($package_type->id))
								<p><strong>Вы выбрали:</strong></p>
								<p>{{ $package_type->title }}</p>
							@endif
							@if(isset($package->id))
								<p>{{ $package->title }}</p>
							@endif
						</div>
					</div><!-- .form-card__body" -->
					<div class="form-card__footer">
						<nav class="nav-bottom">
							<ul>
								<li class="dropup">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Мы ВКонтакте <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="https://vk.com/fvsu_9may_hm" target="blank">Велопарад Ханты-Мансийск</a></li>
										<li><a href="https://vk.com/fvsu_9may_megion" target="blank">Велопарад Мегион</a></li>
										<li><a href="https://vk.com/fvsu_9may_nijn" target="blank">Велопарад Нижневартовск</a></li>
										<li><a href="https://vk.com/fvsu_9may_mejd" target="blank">Велопарад Междуреченский</a></li>
										<li><a href="https://vk.com/fvsu_9may_surgut" target="blank">Велопарад Сургут</a></li>
										<li><a href="https://vk.com/fvsu_9may_uray" target="blank">Велопарад Урай</a></li>
										<li><a href="https://vk.com/fvsu_9may_poyk" target="blank">Велопарад Пойковский</a></li>
										<li><a href="https://vk.com/fvsu_9may_nyagan" target="blank">Велопарад Нягань</a></li>
										<li><a href="https://vk.com/fvsu_9may_ugorsk" target="blank">Велопарад Югорск</a></li>
										<li><a href="https://vk.com/fvsu_9may_neft" target="blank">Велопарад Нефтеюганск</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>

		</div>
	</div>
</div>

@endsection