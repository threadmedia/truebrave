@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

			{{ Form::open(array('url' => '/profile/package-select', 'method' => 'post', 'id' => 'package-select-form')) }}
				<input type="hidden" id="package_type_id" name="package_type_id">
				<div class="form-card">
					<div class="form-card__header">
						<h2>Регистрация | Шаг 3</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<h3>Выберите пакет</h3>

						@foreach($event->package_types as $package)
							@if($package->id == 2)
								@if(date('Ymd') < 20180507)
									<div class="radio-box" data-id="{{ $package->id }}">
										<div class="radio-box__button">
											<span class="radio-box__circle"></span>
										</div>
										<div class="radio-box__inner">
											<h3>{{ $package->title }} - @if($package->price > 0) {{ $package->price }} Руб @else Бесплатно @endif</h3>
											{!! $package->description !!}
											<a href="#" data-toggle="modal" data-target="#info-package-{{$package->id}}">Посмотреть содержимое</a>
										</div>
									</div>
								@endif
							@else
								<div class="radio-box" data-id="{{ $package->id }}">
									<div class="radio-box__button">
										<span class="radio-box__circle"></span>
									</div>
									<div class="radio-box__inner">
										<h3>{{ $package->title }} - @if($package->price > 0) {{ $package->price }} Руб @else бесплатно @endif</h3>
										{!! $package->description !!}
										<a href="#" class="hidden" data-toggle="modal" data-target="#info-package-{{$package->id}}">Посмотреть содержимое</a>
									</div>
								</div>
							@endif
						@endforeach

						<div class="subtitle hidden">
							<h4>Первым зарегистрировавшимся пакет "На память" бесплатно"</h4>
							<span class="amount">{{ $free_package->amount }}</span>
						</div>
						
						@if(Session::has('free_limit_excided'))
							<div class="subtitle">
								<p>К сожалению, бесплатные пакеты закончились.</p>
							</div>
						@endif

						<div class="form-group text-center">
							<a href="#"class="hidden" data-toggle="modal" data-target="#info-get-package">Как получить сувенирную футболку?</a>
						</div>

						<div class="form-group text-center">
							<a href="#" class="hidden" data-toggle="modal" data-target="#info-lgots">Социальные льготы для отдельных категорий граждан</a>
						</div>

						<div class="form-group text-center">
							<button type="submit" class="btn btn-default" id="submit-act" disabled>Далее</button>
						</div>
					</div><!-- .form-card__body" -->
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>

	<div class="modal fade" id="info-get-package" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Как получить футболку?</h4>
				</div>
				<div class="modal-body">
					<p>Футболка</p>
					<p>О точном времени и месте старта вы будете уведомлены позднее по Email, который вы указывали при регистрации.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="info-lgots" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Социальные льготы для отдельных категорий граждан</h4>
				</div>
				<div class="modal-body">
					<p>Участники с ограниченными возможностями (I и II группы, дети-инвалиды), участники уважаемого возраста (более 65) и Герои России могут получить сувенирный пакет бесплатно по предварительной заявке, отправленной на адрес fvspress@yandex.ru до 4го мая. Количество льготных пакетов ограничено.</p>
				</div>
			</div>
		</div>
	</div>

	@foreach($event->package_types as $package)
		<div class="modal fade" id="info-package-{{$package->id}}" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Содержимое пакета "{{ $package->title }}"</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="/img/{{ $package->thumb }}" alt="{{ $package->title }}" title="Содержимое пакета {{ $package->title }}" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach

@endsection