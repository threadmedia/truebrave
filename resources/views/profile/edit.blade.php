@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

			{{ Form::open(array('url' => 'profile-info', 'method' => 'post', 'id' => 'profile-info-form')) }}
				<div class="form-card">
					<div class="form-card__header">
						<h2>Регистрация | Шаг 1</h2>
						{{ Form::select('territory_id', $territory_list, $territory_id, ['class' => 'form-control', 'required' => 'required']) }}
						@if ($errors->has('territory_id'))
							<span class="help-block">
								<strong>{{ $errors->first('territory_id') }}</strong>
							</span>
						@endif
					</div>
					@if(Auth::guest())
						<div class="info-section text-center">
							<p>Если вы ранее регистрировались,<br>то можете <a href="#" id="request-email">повторно запросить письмо</a> со ссылкой <br>для авторизации в системе.</p>
						</div>
						<div class="info-section d-none" id="request-form">
							<div class="input-group">
								<input type="email" class="form-control" name="request-email-control" id="request-email-control">
								<span class="input-group-btn">
									<button class="btn btn-default" id="request-act" type="button">Отправить</button>
								</span>
							</div><!-- /input-group -->
						</div>
					@endif
					<div class="form-card__body">
						<div class="form-group">
							<label for="email">Электронная почта</label>
							@if(Auth::guest())
								{{ Form::email('email', old('email'), ['class' => 'form-control required', 'required' => 'required', 'placeholder' => 'Введите ваш адрес электронной почты']) }}
							@else
								{{ Form::email('email', Auth::user()->email, ['class' => 'form-control required', 'disabled' => 'disabled', 'required' => 'required', 'placeholder' => 'Введите ваш адрес электронной почты']) }}
							@endif

							@if ($errors->has('email'))
								<span class="help-block">
									<strong>{{ $errors->first('email') }}</strong>
								</span>
							@endif
						</div>

						<div class="form-group">
							<label for="last_name">Фамилия</label>
							{{ Form::text('last_name', old('last_name'), ['class' => 'form-control required', 'required' => 'required', 'placeholder' => 'Введите вашу фамилию']) }}
						</div>
						@if ($errors->has('last_name'))
							<span class="help-block">
								<strong>{{ $errors->first('last_name') }}</strong>
							</span>
						@endif

						<div class="form-group">
							<label for="first_name">Имя</label>
							{{ Form::text('first_name', old('first_name'), ['class' => 'form-control required', 'required' => 'required', 'placeholder' => 'Введите ваше имя']) }}
						</div>
						@if ($errors->has('first_name'))
							<span class="help-block">
								<strong>{{ $errors->first('first_name') }}</strong>
							</span>
						@endif

						<div class="form-group">
							<label for="patronymic">Отчество</label>
							{{ Form::text('patronymic', old('patronymic'), ['class' => 'form-control', 'placeholder' => 'Введите ваше отчество (необязательно)']) }}
						</div>

						<div class="form-group">
							<label for="birthdate">Дата рождения</label>
							{{ Form::text('birthdate', old('birthdate'), ['class' => 'form-control required', 'required' => 'required', 'placeholder' => 'Формат дд.мм.гггг (пример 14.10.1985)']) }}
							@if ($errors->has('birthdate'))
								<span class="help-block">
									<strong>{{ $errors->first('birthdate') }}</strong>
								</span>
							@endif
						</div>

						<div class="checkbox">
							<label for="fz152">
								{{ Form::checkbox('fz152', old('fz152'), true) }}	Я ознакомлен и соглашаюсь с <a href="/file/Policy_of_personal_data_processing.pdf" target="blank">политикой обработки персональных данных</a> (152-ФЗ)
							</label>
							@if ($errors->has('fz152'))
								<span class="help-block">
									<strong>{{ $errors->first('fz152') }}</strong>
								</span>
							@endif
						</div>

						<div class="checkbox">
							<label for="is_informed">
								{{ Form::checkbox('is_informed', old('is_informed'), true) }} Я обязуюсь соблюдать ПДД, а также принимаю на себя всю ответственность за свою жизнь, здоровье и возможные травмы, которые я могу получить во время участия в мероприятии.
							</label>
						</div>

						<div class="form-group text-center">
							<button type="submit" class="btn btn-default" id="submit-act" disabled>Отправить</button>
						</div>
					</div>
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>
@endsection