@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

				<div class="form-card">
					<div class="form-card__header">
						<h2>Вы уже зарегистрировались</h2>
					</div>
					<div class="form-card__body">
						<p>Вы уже зарегистрировались на данное мероприятие.</p>
						<p>О месте и времени старта мероприятия вы будете извещены позднее. Мы уведомим Вас по электронной почте.</p> 

						@if(Auth::user()->territory->eventTerritory->is_disabled != 1)
							<p>Распечатайте бланк подтверждения регистрации и отдайте организаторам на старте.</p>
							<div class="push-block">
								<a href="{{ route('profile.get_ticket') }}" target="blank" class="btn btn-block btn-lg btn-default">
									Скачать бланк регистрации
								</a>
								<br>
								<p class="text-center">Копия бланка отправлена Вам на электронную почту.</p>
							</div>
						@endif

						<p>Вы выбрали:</p>
						<p>
							<ul>
								<li>
									<p>{{ $package_user->package_type->title }} 
									@if($package_user->package_type->price > 0) 
										{{ $package_user->package_type->price }} руб. 
									@endif
									</p>
								</li>
								@if($package_user->package_id > 0) 
									<li><p>{{ $package_user->package->title }}</p></li>
								@endif
							</ul>
						</p>
					{{-- 
						<div class="form-group text-center ">
							<a href="#" data-toggle="modal" data-target="#info-get-package">Информация о получении пакета</a>
						</div> 
						--}}
					</div>
					<div class="form-card__footer">
						<iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%B0%D1%82%D1%8C%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82&targets-hint=&default-sum=150&button-text=11&payment-type-choice=on&hint=&successURL=http%3A%2F%2F%D0%B7%D0%B2%D0%B5%D0%B7%D0%B4%D0%B0%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%B8.%D1%80%D1%84%2Fthanks&quickpay=shop&account=410011918297942" width="100%" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
					</div><!-- .form-card__footer" -->

				</div>

		</div>
	</div>
</div>


<div class="modal fade" id="info-get-package" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Информация о получении пакета</h4>
			</div>
			<div class="modal-body">
				<h4>Бесплатный пакет</h4>
				<p>Выдача значков будет организована прямо перед стартом велопарада.</p>
				<p>О точном времени и месте старта вы будете уведомлены позднее по Email, который вы указывали при регистрации.</p>
				<h4>Платный пакет</h4>
				<p>Выдача сувенирных пакетов будет организована ориентировочно в период с 6 по 8 мая. О точном времени и месте выдачи вы будете уведомлены позднее по Email, который вы указывали при регистрации.</p>
			</div>
		</div>
	</div>
</div>

@endsection