@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

				<div class="form-card">
					<div class="form-card__header">
						<h2>ВЫ ЗАРЕГИСТРИРОВАНЫ!</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<h3 class="text-center">ДОПОЛНИТЕЛЬНАЯ ИНФОРМАЦИЯ</h3>
						<p>О месте и времени старта мероприятия вы будете извещены позднее. Мы уведомим Вас по электронной почте.</p> 
						
						@if(Auth::user()->territory->eventTerritory->is_disabled != 1)
							<p>Распечатайте бланк подтверждения регистрации и отдайте организаторам на старте.</p>
							<div class="push-block">
								<a href="{{ route('profile.get_ticket') }}" target="blank" class="btn btn-block btn-lg btn-default">
									Скачать бланк регистрации
								</a>
								<br>
								<p class="text-center">Копия бланка отправлена Вам на электронную почту.</p>
							</div>
						@endif

						
						@if(Auth::id())
							@php
							  $user_package = Auth::user()->user_packages()->first();
								if(isset($user_package->id)) {
									$package_type = App\PackageType::find($user_package->package_type_id);
									$package = App\Package::find($user_package->package_id);
								}
							@endphp
						@endif

						<div>
							@if(isset($package_type->id))
								<p><strong>Вы выбрали:</strong></p>
								<p>{{ $package_type->title }}</p>
							@endif
							@if(isset($package->id))
								<p>{{ $package->title }}</p>
							@endif
						</div>
					</div><!-- .form-card__body" -->
					<div class="form-card__footer section-quickpay">
							<iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%B0%D1%82%D1%8C%20%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82&targets-hint=&default-sum=150&button-text=11&payment-type-choice=on&hint=&successURL=http%3A%2F%2F%D0%B7%D0%B2%D0%B5%D0%B7%D0%B4%D0%B0%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D0%B8.%D1%80%D1%84%2Fthanks&quickpay=shop&account=410011918297942" width="100%" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
					</div><!-- .form-card__footer" -->
					<div class="form-card__footer">
						<nav class="nav-bottom">
							<ul>
								<li class="dropup">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Мы ВКонтакте <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="https://vk.com/fvsu_9may_hm" target="blank">Велопарад Ханты-Мансийск</a></li>
										<li><a href="https://vk.com/fvsu_9may_surgut" target="blank">Велопарад Сургут</a></li>
										<li><a href="https://vk.com/fvsu_9may_uray" target="blank">Велопарад Урай</a></li>
										<li><a href="https://vk.com/fvsu_9may_poyk" target="blank">Велопарад Пойковский</a></li>
										<li><a href="https://vk.com/fvsu_9may_mejd" target="blank">Велопарад Междуреченский</a></li>
										<li><a href="https://vk.com/fvsu_9may_vysokiy" target="blank">Велопарад Высокий</a></li>
										<li><a href="https://vk.com/fvsu_9may_nyagan" target="blank">Велопарад Нягань</a></li>
										<li><a href="https://vk.com/fvsu_9may_oktyabrskoe" target="blank">Велопарад Октябрьское</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>

		</div>
	</div>
</div>

@endsection