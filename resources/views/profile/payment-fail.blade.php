@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

				<div class="form-card">
					<div class="form-card__header">
						<h2>Что-то пошло не так</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<h3 class="text-center">Во время проведения платежа чтото-пошло не так и платеж не был выполнен.</h3>
            <p>Пожалуйста, попробуйте еще раз провести оплату.</p>
            <p>Если возникнут какие-либо вопросы, вы всегда можете связаться с нами через данные, указанные на <a href="/page/contacts">странице контактов.</a></p>
					</div><!-- .form-card__body" -->
					<div class="form-card__footer">
						<nav class="nav-bottom">
							<ul>
								<li class="dropup">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Мы ВКонтакте <span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="https://vk.com/fvsu_9may_hm" target="blank">Велопарад Ханты-Мансийск</a></li>
										<li><a href="https://vk.com/fvsu_9may_megion" target="blank">Велопарад Мегион</a></li>
										<li><a href="https://vk.com/fvsu_9may_nijn" target="blank">Велопарад Нижневартовск</a></li>
										<li><a href="https://vk.com/fvsu_9may_mejd" target="blank">Велопарад Междуреченский</a></li>
										<li><a href="https://vk.com/fvsu_9may_surgut" target="blank">Велопарад Сургут</a></li>
										<li><a href="https://vk.com/fvsu_9may_uray" target="blank">Велопарад Урай</a></li>
										<li><a href="https://vk.com/fvsu_9may_poyk" target="blank">Велопарад Пойковский</a></li>
										<li><a href="https://vk.com/fvsu_9may_nyagan" target="blank">Велопарад Нягань</a></li>
										<li><a href="https://vk.com/fvsu_9may_ugorsk" target="blank">Велопарад Югорск</a></li>
										<li><a href="https://vk.com/fvsu_9may_neft" target="blank">Велопарад Нефтеюганск</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>

		</div>
	</div>
</div>

@endsection