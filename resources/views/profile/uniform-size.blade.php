@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

			{{ Form::open(array('url' => '/profile/uniform-size', 'method' => 'post', 'id' => 'uniform-size-form')) }}
				<input type="hidden" id="package_id" name="package_id">
				<div class="form-card">
					<div class="form-card__header">
						<h2>Регистрация | Шаг 3</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<h3>Выбор размера футболки</h3>

						@foreach($packages as $package)
							<div class="radio-box" data-id="{{ $package->id }}">
								<div class="radio-box__button">
									<span class="radio-box__circle"></span>
								</div>
								<div class="radio-box__inner">
									<h4>{{ $package->title }}</h4>
									<span class="amount">{{ $package->pivot->amount }}</span>
								</div>
							</div>
						@endforeach

						<div class="form-group text-center">
						</div>

						<div class="form-group text-center">
							<a href="{{ route('profile.package-select')}}" class="btn btn-default">Назад</a>
							<button type="submit" class="btn btn-primary" id="submit-act" disabled>Далее</button>
						</div>
					</div><!-- .form-card__body" -->
				</div>
			{{ Form::close() }}

		</div>
	</div>
</div>

	<div class="modal fade" id="info-lgots" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Социальные льготы для отдельных категорий граждан</h4>
				</div>
				<div class="modal-body">
					<p>Участники с ограниченными возможностями (I и II группы, дети-инвалиды), участники уважаемого возраста (более 65) и Герои России могут получить сувенирный пакет бесплатно по предварительной заявке, отправленной на адрес email@email.ru до 4го мая. Количество льготных пакетов ограничено.</p>
				</div>
			</div>
		</div>
	</div>

	@foreach($event->package_types as $package)
		<div class="modal fade" id="info-package-{{$package->id}}" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Содержимое пакета "{{ $package->title }}"</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="/img/{{ $package->thumb }}" alt="{{ $package->title }}" title="Содержимое пакета {{ $package->title }}" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach

@endsection