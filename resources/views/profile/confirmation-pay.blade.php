@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">

			<a href="#" class="logo-link mini-logo">
				<img src="/img/logo.png" class="logo img-responsive">
			</a>

			{{-- {{ Form::open(array('url' => '/profile/uniform-size', 'method' => 'post', 'id' => 'uniform-size-form')) }} --}}
				<input type="hidden" id="package_id" name="package_id">
				<div class="form-card">
					<div class="form-card__header">
						<h2>Регистрация | Шаг 3</h2>
					</div><!-- .form-card__header" -->
					<div class="form-card__body">
						<p><strong>Вы выбрали:</strong>
							<ul>
								<li><strong>Территория:</strong> {{ Auth::user()->territory->title }}</li>
								<li><strong>{{ $package_type->title}}</strong></li>
								@if(isset($package))
									<li><strong>{{ $package->title }} - {{ $package_type->price }} руб.</strong></li>
								@endif
							</ul>
						</p>
					</div><!-- .form-card__body" -->

					<div class="form-card__footer">
						<div class="text-center">
							<a href="{{ route('profile.package-select')}}" class="btn btn-default">Назад</a>
							@if(isset($package))
								 @if($free_package && $free_package->amount > 0)
									{{ Form::open(array('url' => '/profile/confirmation', 'method' => 'post', 'class' => "d-inline-block")) }}
										<button type="submit" class="btn btn-primary" id="submit-act">Получить пакет</button>
									{{ Form::close() }}
								@else
									@include('yandex_kassa::form')
								@endif
								{{-- <button type="submit" class="btn btn-primary" id="submit-act">Перейти к оплате</button> --}}
							@else
								<button type="submit" class="btn btn-primary" id="submit-act">Подтвердить</button>
							@endif
						</div>
					</div>
				</div>
			{{-- {{ Form::close() }} --}}

		</div>
	</div>
</div>

	<div class="modal fade" id="info-lgots" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Социальные льготы для отдельных категорий граждан</h4>
				</div>
				<div class="modal-body">
					<p>Участники с ограниченными возможностями (I и II группы, дети-инвалиды), участники уважаемого возраста (более 65) и Герои России могут получить сувенирный пакет бесплатно по предварительной заявке, отправленной на адрес email@email.ru до 4го мая. Количество льготных пакетов ограничено.</p>
				</div>
			</div>
		</div>
	</div>

@endsection