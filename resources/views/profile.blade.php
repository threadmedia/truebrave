@extends('layouts.console')
@section('content')

<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading text-bold">Данные профиля</div>
			<div class="panel-body">
				{!! Form::open(['url' => 'profile', 'method' => 'post']) !!}
					@if(Auth::user()->verified)
						<div class="form-group">
							<div class="input-group">
								{{ Form::text('email', Auth::user()->email, ['class' => 'form-control', 'disabled' => 'disabled']) }}
								<span class="input-group-addon" title="Ваш адрес электронной почты подтвержден">
									<span class="glyphicon glyphicon-ok color-green"></span>
								</span>
							</div>
						</div>
					@else
						<div class="form-group">
							{{ Form::text('email', Auth::user()->email, ['class' => 'form-control', 'disabled' => 'disabled']) }}
						</div>
					@endif
					<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
						{{ Form::text('phone', Auth::user()->phone, ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Укажите номер вашего сотового телефона', 'required' => 'required']) }}
						@if ($errors->has('phone'))
							<span class="help-block">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group">
						{{ Form::text('first_name', Auth::user()->first_name, ['class' => 'form-control',  'id' => 'first_name',  'placeholder' => 'Укажите ваше Имя']) }}
					</div>
					<div>
						<button type="submit" class="btn btn-success">Сохранить</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>

		@if(!Auth::user()->verified)
			<div class="panel panel-warning">
				<div class="panel-heading text-bold">Подтверждение Email адреса</div>
				<div class="panel-body">
					<p>Вы еще не подтвердили ваш адрес электронной почты.</p>
					<p>При регистрации вам было выслано письмо с ссылкой на подтверждение.</p>
					<p>Если по каким-то причинам, вы не получили письмо, вы можете отправить себе новый запрос на подтверждение</p>
				</div>
				<div class="panel-footer">
					{{--  <a href="/profile/verify-email" class="btn btn-block btn-warning">Отправить письмо для подтверждения</a>  --}}
				</div>
			</div>
		@endif

	</div>
</div>
@endsection