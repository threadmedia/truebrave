@extends('layouts.app')
@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default  panel-front">
				<div class="panel-heading">Форма регистрации</div>

				<div class="panel-body">
					{{--  <form class="form-horizontal" method="POST" action="{{ route('register') }}">  --}}
					<form class="form-horizontal" method="POST" action="{{ route('signupPost') }}">
						{{ csrf_field() }}

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-md-4 control-label">Email адрес</label>
							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
							<label for="password" class="col-md-4 control-label">Пароль</label>
							<div class="col-md-6">
								<input id="password" type="password" class="form-control" name="password" required>
								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="password-confirm" class="col-md-4 control-label">Повторите пароль</label>
							<div class="col-md-6">
								<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>
						<div class="form-group">

						<div class="checkbox{{ $errors->has('agreement') ? ' has-error' : '' }}">
							<div class="col-md-offset-4 col-md-6">
								<label>
									<input id="agreement" type="checkbox" name="agreement" checked required> согласие на обработку персональных данных 
								</label>
								@if ($errors->has('password'))
									<span class="help-block">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<br>

						<div class="form-group">
							<div class="col-md-offset-4 col-md-6">
								{!! NoCaptcha::renderJs() !!}
								{!! NoCaptcha::display() !!}
								@if ($errors->has('g-recaptcha-response'))
									<span class="help-block">
										<strong>Вы не отметили reCaptcha</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-success">Зарегистрироваться</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
