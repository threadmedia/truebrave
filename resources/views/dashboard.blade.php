@extends('layouts.console')
@section('content')

<div class="row">
	<div class="col-lg-4">
		@include('widgets.message-box')
	</div>

	<div class="col-lg-4 hidden">
		@if(!Auth::user()->verified)
			@include('widgets.email-not-verified')
		@endif
	</div>

	<div class="col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading text-bold">Сводка</div>
			<div class="panel-body">
				Число аккаунтов: {{ $accounts->count()}}
			</div>
		</div>

		<div class="panel panel-default hidden">
			<div class="panel-heading text-bold">Контактная информация</div>
			<div class="panel-body text-center">
				8-800-511-06-78
			</div>
			<div class="panel-footer">
				<a href="/contacts" class="btn btn-block btn-default">Подробнее</a>
			</div>
		</div>
	</div>
</div>
@endsection