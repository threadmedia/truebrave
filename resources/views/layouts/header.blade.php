<header class="header">
	<div class="container-fluid">
		<nav>
			@php
				$prefix = (Route::currentRouteName() == 'site.order') ? config('app.url') : '';
			@endphp
			<div class="header__list-wrap">
				<ul class="header__menu-list">
					<li><a href="{{$prefix}}#about">О забеге</a></li>
					<li><a href="{{$prefix}}#event-place">Место проведения</a></li>
					<li><a href="{{$prefix}}#footer">Организаторы</a></li>
					<li><a href="{{$prefix}}#event-program">Программа</a></li>
					<li><a href="{{$prefix}}#registration">Регистрация</a></li>
					@guest
						<li><a href="#signin-section-anchor" class="signin-link">Войти</a></li>
					@endguest
					@auth
						<li><a href="{{ route('site.order')}}" class="order-link">Оплата</a></li>
						@if(Auth::user()->role === 'admin')
							<li><a href="{{ route('user.index')}}" class="order-link">Участники</a></li>
						@endif
					@endauth
				</ul>
			</div>

			<!-- Mobile menu -->
			<div class="header__hamburger-toggle"><span></span></div>
			<div class="header__menu-slide">
				<div class="menu-slide__logo">
					<img src="img/logo-menu.svg" alt="Logo">
				</div>
				<div class="menu-slide__btn-close header__hamburger-toggle"></div>
				<ul class="menu-slide__list" id="mobile-menu-list">
					<li><a href="{{$prefix}}#about">О забеге</a></li>
					<li><a href="{{$prefix}}#event-place">Трасса</a></li>
					<li><a href="{{$prefix}}#footer">Организаторы</a></li>
					<li><a href="{{$prefix}}#event-program">Программа</a></li>
					<li><a href="{{$prefix}}#registration">Регистрация</a></li>
					@guest
						<li><a href="#signin-section-anchor" class="signin-link">Войти</a></li>
					@endguest
					@auth
						<li><a href="{{ route('site.order')}}" class="order-link">Оплата</a></li>
						@if(Auth::user()->role === 'admin')
							<li><a href="{{ route('user.index')}}" class="order-link">Участники</a></li>
						@endif
					@endauth
				</ul>
				<span class="menu-slide__phone-desc">Контактный номер телефона</span>
				<a href="tel:88003500176" class="menu-slide__phone-number">8 800 3500 176</a>
				<ul class="menu-slide__social">
					<li>
						<a href="https://vk.com/truebrave" target="_blank">
							<img src="{{ config('app.url') }}/img/icons/vk-icon.svg" alt="Мы в Vk">
						</a>
					</li>
					<li>
						<a href="https://www.facebook.com/groups/marafonhm/" target="_blank">
							<img src="{{ config('app.url') }}/img/icons/facebook-icon.svg" alt="Мы в Facebook">
						</a>
					</li>
					<li>
						<a href="https://www.instagram.com/sportfondhm/" target="_blank">
							<img src="{{ config('app.url') }}/img//icons/instagram-icon.svg" alt="Мы в Instagram">
						</a>
					</li>
					<li>
						<a href="http://www.sportfond.team/" target="_blank">
							<img src="{{ config('app.url') }}/img/icons/support-fund-menu.svg" alt="Спорт фонд">
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<div class="cover-filter"></div>
</header>