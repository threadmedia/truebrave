<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Дело Храбрых - Экстремальный забег</title>
	<link href="{{ asset('css/bootstrap.min.css') }}?v={{ config('app.version') }}" rel="stylesheet">
	<link href="{{ asset('css/style.back.css') }}?v={{ config('app.version') }}" rel="stylesheet">
	<link href="{{ asset('css/datepicker.min.css') }}?v={{ config('app.version') }}" rel="stylesheet">

	<script src="{{ config('app.url') }}/js/vendors/vue.2.6.9.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/axios.min.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/jquery-3.4.1.min.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/bootstrap.min.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/jquery.mask.min.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/datepicker/datepicker.min.js?v={{ config('app.version') }}"></script>
	<script src="{{ config('app.url') }}/js/vendors/datepicker/i18n/datepicker.ru-RU.js?v={{ config('app.version') }}"></script>

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/img/faicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/img/faicon/favicon-16x16.png" sizes="16x16" />

	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#000000" />
	<meta name="msapplication-TileImage" content="{{ config('app.url') }}/img/faicon/mstile-144x144.png" />
</head>

<body>
	<div class="wrapper">
		<header>
			<div class="container-fluid">
				<nav>
					<a href="{{ config('app.url') }}">На главную</a>
				</nav>
			</div>
		</header>
		<section class="main">
			@yield('content')
		</section>
	</div>
</body>
</html>