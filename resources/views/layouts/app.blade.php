<!DOCTYPE html>
<html lang="ru">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	@auth
	<script>
		var routes = {
			'payment': {
				'pay': '{{ route('payment.pay') }}',
				'check': '{{ route('payment.check') }}',
			}
		};	
	</script>

	@endauth
	<meta property="og:locale" content="ru_RU"/>
	<meta property="og:type" content="article"/>
	<meta property="og:title" content="Экстремальный забег «Дело храбрых»"/>
	{{-- <meta property="og:description" content=""/> --}}
	<meta property="og:image" content="{{ config('app.url') }}/img/faicon/mstile-144x144.png"/>
	<meta property="og:url" content="{{ config('app.url') }}/{{Request::path()}}"/>
	<meta property="og:site_name" content="{{ config('app.name', 'Laravel') }}"/>

	<title>Дело Храбрых - Экстремальный забег</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap&subset=cyrillic" rel="stylesheet">

	<link href="{{ asset('css/fonts.css') }}?v={{ config('app.version') }}" rel="stylesheet">
	<link href="{{ asset('css/bootstrap.min.css') }}?v={{ config('app.version') }}" rel="stylesheet">
	<link href="{{ asset('css/bootstrap.min.css.map') }}?v={{ config('app.version') }}" rel="stylesheet">

	<link href="{{ asset('css/style.css') }}?v={{ config('app.version') }}" rel="stylesheet">
	<link href="{{ asset('css/datepicker.min.css') }}?v={{ config('app.version') }}" rel="stylesheet">

	<script src="{{ config('app.url') }}/js/vendors/axios.min.js?v={{ config('app.version') }}"></script>

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ config('app.url') }}/img/faicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/img/faicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/img/faicon/favicon-16x16.png" sizes="16x16" />

	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#000000" />
	<meta name="msapplication-TileImage" content="{{ config('app.url') }}/img/faicon/mstile-144x144.png" />
</head>

<body>
	<div class="wrapper">
		@include('layouts.header')

		{{-- @auth --}}
			@include('layouts.stickybar')
		{{-- @endauth --}}

		<section class="main">
			@yield('content')
		</section>

		@include('layouts.footer')
	</div>
</body>
</html>