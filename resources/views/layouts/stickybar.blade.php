<div class="header__sticky-bar">
  <a href="http://www.sportfond.team/" class="sticky-bar__fund-logo" target="_blank">
    <img src="{{ config('app.url') }}/img/icons/support-fund.svg" alt="Спорт фонд">
  </a>
  <ul class="sticky-bar__social">
    <li>
      <a href="https://vk.com/truebrave" target="_blank">
        <img src="{{ config('app.url') }}/img/icons/vk-icon.svg" alt="Мы в Vk">
      </a>
    </li>
    <li>
      <a href="https://www.facebook.com/sportfondhm/" target="_blank">
        <img src="{{ config('app.url') }}/img/icons/facebook-icon.svg" alt="Мы в Facebook">
      </a>
    </li>
    <li>
      <a href="https://www.instagram.com/sportfondhm/" target="_blank">
        <img src="{{ config('app.url') }}/img/icons/instagram-icon.svg" alt="Мы в Instagram">
      </a>
    </li>
  </ul>
  <div class="sticky-bar-phone">
    <span class="sticky-bar__phone-desc">Контактный номер телефона</span>
    <a href="tel:88003500176" class="sticky-bar__phone-number">8 800 3500 176</a>
  </div>
</div>