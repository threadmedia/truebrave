<footer class="footer" id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="organizers">
					<h2 class="title footer__title">Организаторы</h2>
					<div class="footer__img">
						<a href="http://www.sportfond.team/" target="_blank" class="organizers__img">
							<img src="{{ config('app.url') }}/img/icons/support-fund.svg" alt="Fund">
						</a>
					</div>
				</div>
			</div>

			<div class="col-xs-12">
				<div class="partners">
					<h2 class="title footer__title">Партнеры</h2>
					<div class="row partners-list">
						<div class="col-xs-12 col-md-4">
							<a href="https://admhmao.ru" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/admhmao.png" alt="TM logo">
							</a>
						</div>
						<div class="col-xs-12 col-md-4">
							<a href="https://admhmansy.ru" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/admhmansy.png" alt="TM logo">
							</a>
						</div>
						<div class="col-xs-12 col-md-4">
							<a href="http://ugranko.ru/" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/socialfond.png" alt="TM logo">
							</a>
						</div>
					</div>

					<div class="row partners-list">
						<div class="col-xs-12 col-md-4">
							<a href="https://hanty.rt.ru/" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/rostelekom.png" alt="TM logo">
							</a>
						</div>
						<div class="col-xs-12 col-md-4">
							<a href="https://www.ugsk.ru/" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/ugoria.png" alt="TM logo">
							</a>
						</div>
						<div class="col-xs-12 col-md-4">
							<a href="http://www.tm-ss.ru/" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/icons/tm-logo.svg" alt="TM logo">
							</a>
						</div>
					</div>

					<div class="row partners-list">
						<div class="col-xs-12 col-md-4 col-md-offset-4">
							<a href="https://rmc-ugra.ru" target="_blank" class="partners-item">
								<img src="{{ config('app.url') }}/img/rmc_logo.png" alt="TM logo">
							</a>
						</div>
					</div>


				</div>
			</div>
		</div>

		<div class="row info-list">
			<div class="col-xs-12">
				<p>Предварительная подготовка участников к экстремальному забегу:<br></p>
				<p><strong>ИП Шаригина К.Г.</strong><br><br></p>
				<p>ОГРНИП: 316861700086069<br>
				ИНН: 861006436490<br>
				Email: sportfondhm@yandex.ru</p>
			</div>
		</div>
		
		<div class="row doc-list">
			<div class="col-xs-12">
				<a href="{{ config('app.url') }}/docs/user_agreement.pdf" target="blank">Пользовательское соглашение</a>
				<a href="{{ config('app.url') }}/docs/public_offer.pdf" target="blank">Публичная оферта</a>
				<a href="{{ config('app.url') }}/docs/privacy_policy.pdf" target="blank">Политика конфеденциальности</a>
				<a href="{{ config('app.url') }}/docs/pricelist.pdf" target="blank">Прейскурант</a>
			</div>
		</div>

		<div class="text-center" style="color: #111">
			{{Carbon\Carbon::now()}}
		</div>
	</div>
</footer>

<div id="toTop"></div>

<script src="{{ config('app.url') }}/js/vendors/jquery-2.2.4.min.js?v={{ config('app.version') }}"></script>
{{-- <script src="{{ config('app.url') }}/js/vendors/jquery-3.4.1.min.js?v={{ config('app.version') }}"></script> --}}
<script src="{{ config('app.url') }}/js/vendors/jquery.cookie.min.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/vendors/bootstrap.min.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/vendors/jquery.mask.min.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/vendors/datepicker/datepicker.min.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/vendors/datepicker/i18n/datepicker.ru-RU.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/vendors/axios.min.js?v={{ config('app.version') }}"></script>
<script src="{{ config('app.url') }}/js/app.js?v={{ config('app.version') }}"></script>

@if(Route::currentRouteName() != 'site.order')
<script src="{{ config('app.url') }}/js/main.js?v={{ config('app.version') }}"></script>
@endif

@guest
	<script src="{{ config('app.url') }}/js/registration.js?v={{ config('app.version') }}"></script>
@endguest
@auth
	<script src="{{ config('app.url') }}/js/payment.js?v={{ config('app.version') }}"></script>
@endauth

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
	 (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		 m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		 (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
		 ym(54537526, "init", {
			clickmap:true,
			trackLinks:true,
			accurateTrackBounce:true
		 });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/54537526" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->