@extends('layouts.console')
@section('content')

<style>
  .faded {
    opacity: 0.3;
  }
  .faded td {
    background: #ddd !important;
  }
</style>

<section class="container-fluid" id="app">
  <button class="btn btn-default" @click="prerollTest()">
    Тест письма
  </button>

  <div class="row">
    <div class="col-xs-12">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="text-center">ID</th>
            <th width="60px">№</th>
            <th>Название команды</th>
            <th width="120px">Unique Код</th>
            <th>ФИО капитана</th>
            <th width="120px">ДР капитана</th>
            <th width="200px">Email</th>
            <th width="200px">Город</th>
            <th>Состав</th>
            <th width="250px">Оплата</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="(user, uk) in getUserList()" :key="uk.id+'uk'" :class="getRowClass(user)">
            <td class="text-center">
              {~ user.id ~}
            </td>
            <td>
              <span v-if="user.team_number > 0">
                № {~ user.team_number ~}
              </span>
            </td>
            <td>
              <a class="btn-link" @click="edit(uk)">
                <strong>{~ user.team_name ~}</strong>
              </a>
            </td>
            <td>
              <span v-if="user.team_number > 0">
                {~ user.code ~}
              </span>
            </td>
            <td>{~ user.cap_fio ~}</td>
            <td>{~ user.birthdate ~}</td>
            <td>
              <div>{~ user.email ~}</div>
              <div>{~ user.phone ~}</div>
            </td>
            <td>{~ user.city ~}</td>
            <td>
              <template v-if="user.teamates.length > 0">
                <ul>
                  <li v-for="(teamate, tk) in user.teamates" :key="tk.id+'tk'">
                    {~ teamate.fio ~} {~ teamate.birthdate ~}
                  </li>
                </ul>
              </template>
            </td>
            <td style="font-size: 11px">
              <template v-if="user.payments.length > 0">
                <div v-for="(payment, pk) in user.payments" :key="pk.id+'pk'">
                  {~ payment.amount ~} руб. <span :class="getStatusLabel(payment.status)">{~ payment.status ~}</span>
                  <div>
                    {~ payment.approved_at ~}
                  </div>
                  <div>
                    {~ payment.pay_id ~}
                  </div>
                </div>
              </template>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="modal" id="user-dialog" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Конанда: {~ current_user.team_name ~}</h4>
        </div>

        <div class="panel panel-default" v-if="typeof current_user.team_number !== 'undefined' && current_user.team_number !== null">
          <div class="panel-body">
            <button type="button" class="btn btn-primary" :disabled="loading" @click="send_confirm()">Продублировать письмо об оплате</button>

            <button type="button" class="btn btn-primary" :disabled="loading" @click="preroll()">Финальное письмо</button>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading text-bold">Команда / №</div>
          <div class="panel-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="team_name" class="col-sm-4 control-label">Название команды</label>
                <div class="col-sm-4">
                  <input type="text" name="team_name" v-model="current_user.team_name" class="form-control">
                </div>
                <div class="col-sm-4">
                  <input type="number" name="team_number" v-model="current_user.team_number" class="form-control">
                </div>
              </div>
  
              <div class="form-group">
                <label for="city" class="col-sm-4 control-label">Город</label>
                <div class="col-sm-4">
                  <input type="text" name="city" v-model="current_user.city" class="form-control">
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="panel panel-default">
          <div class="panel-heading text-bold">Капитан команды</div>
          <div class="panel-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="city" class="col-sm-4 control-label">ФИО / Дата рождения</label>
                <div class="col-sm-4">
                  <input type="hidden" name="cap_id" v-model="current_user.id" class="form-control">
                  <input type="text" name="cap_fio" v-model="current_user.cap_fio" class="form-control">
                </div>
                <div class="col-sm-4">
                  <input type="text" name="birthdate" v-model="current_user.birthdate" class="form-control">
                </div>
              </div>
  
              <div class="form-group">
                <label for="email" class="col-sm-4 control-label">Email / Телефон</label>
                <div class="col-sm-4">
                  <input type="email" name="email" v-model="current_user.email" class="form-control">
                </div>
                <div class="col-sm-4">
                  <input type="text" name="phone" v-model="current_user.phone" class="form-control">
                </div>
              </div>
            </form>
          </div>
        </div>

        <div class="panel panel-default" v-if="typeof current_user.teamates !== 'undefined' && current_user.teamates.length > 0">
          <div class="panel-heading text-bold">Тимэйты</div>
          <div class="panel-body">
            <ul>
              <li v-for="(cteamate, ctk) in current_user.teamates" :key="ctk.id+'ctk'">
                {~ cteamate.fio ~} {~ cteamate.birthdate ~} <button type="button" class="btn btn-primary btn-xs" :disabled="loading" @click="editUserModal(cteamate.user_id, cteamate.id)">Редактировать</button>
              </li>
            </ul>
          </div>
        </div>

        <div class="modal-footer">
          <div class="row">
            <div class="col-xs-6 text-left">
              <button type="button" class="btn btn-default" @click="close()">Закрыть</button>
            </div>
            <div class="col-xs-6">
              <button type="button" class="btn btn-primary" @click="saveEdit()" :disabled="loading">Сохранить</button>
            </div>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

 <div class="modal" id="edit-user-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Пользователь: {~ current_user_edit.fio ~}</h4>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading text-bold">Данные пользователя</div>
          <div class="panel-body">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="city" class="col-sm-4 control-label">ФИО / Дата рождения</label>
                <div class="col-sm-4">
                  <input type="hidden" name="teamate_id" v-model="current_user_edit.id" class="form-control">
                  <input type="text" name="teamate_fio" v-model="current_user_edit.fio" class="form-control">
                </div>
                <div class="col-sm-4">
                  <input type="text" name="birthdate" v-model="current_user_edit.birthdate" class="form-control">
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row">
            <div class="col-xs-6 text-left">
              <button type="button" class="btn btn-default" @click="closeEditUser()">Закрыть</button>
            </div>
            <div class="col-xs-6">
              <button type="button" class="btn btn-primary" @click="saveEditUser()" :disabled="loading">Сохранить</button>
            </div>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

</section>{{-- #app --}}


<script>
const http = axios.create({
  baseURL: '{{ config('app.url') }}/api'
});

var app = new Vue({
  el: '#app',
  delimiters: ['{~', '~}'],
  data: {
    loading: false,
    user_list: [],
    current_user: {},
    current_user_edit: {},
  },
  methods: {
    getUserList() {
      return this.user_list;
    },

    getRowClass(user) {
      return (!user.team_number > 0) ? 'faded' : '';
    },

    getStatusLabel: function (status) {
      let css_class = 'label';
      if(status === 'succeeded') {
        css_class += ' label-success';
      }
      if(status === 'pending') {
        css_class += ' label-default';
      }
      if(status === 'waiting_for_capture') {
        css_class += ' label-info';
      }
      if(status === 'canceled') {
        css_class += ' label-warning';
      }
      return css_class;
    },

    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    },

    edit: function (user_key) {
      this.current_user = this.user_list[user_key];
      $('#user-dialog').modal('show');
    },

    close: function () {
      $('#user-dialog').modal('hide');
    },

    saveEdit: function (user_key) {
        var success = true;
        let that = this;
        var id = $('#user-dialog input[name = "cap_id"]').val();
        var arr = {
            'type' : 'user',
            'cap_fio' : $('#user-dialog input[name = "cap_fio"]').val(),
            'city' : $('#user-dialog input[name = "city"]').val(),
            'birthdate' : $('#user-dialog input[name = "birthdate"]').val(),
            'email' : $('#user-dialog input[name = "email"]').val(),
            'phone' : $('#user-dialog input[name = "phone"]').val(),
            'team_name' : $('#user-dialog input[name = "team_name"]').val(),
        };  
        that.loading = true;
        http.put('/users/'+id, arr, {
                emulateJSON: true
        }).then(function (r) {
            if (r.data.result === 'error') {
              success = false;
              var str = '';
              for (var error in r.data.errors) {
                str += r.data.errors[error]+'\n';
              }
              alert(str);  
            }
            console.log(r.data);
        }).catch(function (error) {
            console.log(error);
        }).finally(function () {
            that.loading = false;
            if (success === true)
              $('#user-dialog').modal('hide');
        });
        
    },

    editUserModal: function (user_id, teamate_id) {
        for (var user_key in this.user_list) {
            if (this.user_list[user_key].id === user_id) {
                for (var teamate_key in this.user_list[user_key].teamates) {
                    if (this.user_list[user_key].teamates[teamate_key].id === teamate_id) {
                        this.current_user_edit = this.user_list[user_key].teamates[teamate_key];
                        $('#edit-user-modal').modal('show');
                        return 1;
                    }
                }
            }
        }
        console.log('teamate by id not found');
    },

    saveEditUser: function () {
        var success = true;
        let that = this;
        var id = $('#edit-user-modal input[name = "teamate_id"]').val();
        var arr = {
            'type' : 'teamate',
            'fio' : $('#edit-user-modal input[name = "teamate_fio"]').val(),
            'birthdate' : $('#edit-user-modal input[name = "birthdate"]').val(),
        };  
        that.loading = true;
        http.put('/users/'+id, arr, {
                emulateJSON: true
        }).then(function (r) {
          if (r.data.result === 'error') {
            success = false;
            var str = '';
            for (var error in r.data.errors) {
              str += r.data.errors[error]+'\n';
            }
            alert(str);  
          }
          console.log(r.data);
        }).catch(function (error) {
            console.log(error);
        }).finally(function () {
            that.loading = false;
            if (success === true)
              $('#edit-user-modal').modal('hide');
        });
    },

    closeEditUser: function () {
        $('#edit-user-modal').modal('hide');
    }, 

    prerollTest: function () {
      let that = this;
      that.loading = true;
      http.get(`/users/prerollTest`)
      .then(function (r) {
        console.log(r.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        that.loading = false;
		  });
    },

    preroll: function () {
      let that = this;
      that.loading = true;
      http.get(`/users/preroll/${that.current_user.id}`)
      .then(function (r) {
        console.log(r.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        that.loading = false;
		  });
    },

    send_confirm: function () {
      let that = this;
      that.loading = true;
      http.post(`/users/${that.current_user.id}`)
      .then(function (r) {
        console.log(r.data);
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        that.loading = false;
		  });
    },
  },

  mounted() {
    $('#user-dialog').modal({
      show: false,
      backdrop: 'static'
    });

    let that = this;
    http.get("{{route('user.list')}}")
		.then(function (r) {
      console.log(r.data);
      that.user_list = r.data.user_list;
		})
		.catch(function (error) {
			console.log(error);
		})
    .finally(function () {
      that.loading = false;
    });
  }
})
</script>
@endsection