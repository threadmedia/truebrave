@extends('layouts.app')
@section('content')

<div class="container-fluid error-404">
	<div class="row pt-4">
		<div class="col-md-8 col-md-offset-2">
			<div class="text-center">
				<h2>Страница, которую вы пытаетесь найти - не существует.</h2>
				<hr>
				<a href="/" class="btn btn-primary text-bold">Вернуться в зону комфорта</a>
			</div>
			<hr>
			<p>Если вы уверены, что тут должна быть нужная вам страница - обратитесь в службу поддержки <strong>contacts@tm-ss.ru</strong></p>
		</div>
	</div>
</div>
@endsection