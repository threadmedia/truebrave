{{--
  -- For more information about form fields
  -- you can visit Yandex Kassa documentation page
  --
  -- @see https://tech.yandex.com/money/doc/payment-solution/payment-form/payment-form-http-docpage/
  --}}
<form action="{{yandex_kassa_form_action()}}" method="{{yandex_kassa_form_method()}}" class="d-inline-block">
	<input name="scId" type="hidden" value="{{yandex_kassa_sc_id()}}">
	<input name="shopId" type="hidden" value="{{yandex_kassa_shop_id()}}">
	<div class="hidden">
		<input name="displaysum" id="yandex_money_displaysum" type="number" value="{{$package_type->price}}" class="form-control" type="hidden">
		<input name="sum" id="yandex_money_sum"  type="hidden" value="{{$package_type->price}}" class="form-control">
		<input name="customerEmail" id="yandex_money_customer_email" value="{{Auth::user()->email}}"  type="text" class="form-control" type="hidden">
		<input name="customerNumber" id="yandex_money_customer_number" value="{{ Auth::user()->email }}" type="hidden">
		<input name="orderNumber" id="yandex_money_order_number" value="c{{Auth::user()->id}}-t{{$package_type->id}}-p{{$package->id}}" type="hidden">
		<input name="cps_email" id="yandex_money_order_number" value="{{Auth::user()->email}}" type="hidden">
	</div>
	<div class="hidden">
		@foreach(yandex_kassa_payment_types() as $paymentTypeCode)
			<input type="radio" name="paymentType" value="{{$paymentTypeCode}}" checked required>
		@endforeach
	</div>
	<button type="submit" class="btn btn-primary">{{trans('yandex_kassa::form.button.pay')}}</button>

</form>