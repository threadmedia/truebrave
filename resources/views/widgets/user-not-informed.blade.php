<div class="panel panel-warning">
  <div class="panel-heading text-bold">Особенности личного кабинета при работе с Экспресс-отчетом:</div>
  <div class="panel-body">
    <ul>
      <li>мы храним сформированный вами Экспресс-отчёт 1 год, по окончании года отчет удаляется, а заявка перенесена в архив;</li>
      <li>повторное формирование Экспресс-отчета по одной и той же компании, возможно не ранее 1 часа с момента ранее сформированного отчета.</li>
    </ul>
  </div>
  <div class="panel-footer">
    <a href="/profile/set-informed" class="btn btn-block btn-warning">Я ознакомлен</a>
  </div>
</div>