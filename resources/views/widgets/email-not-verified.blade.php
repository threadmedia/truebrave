<div class="panel panel-warning">
  <div class="panel-heading text-bold">Подтверждение Email адреса</div>
  <div class="panel-body">
    <p>Вы еще не подтвердили ваш адрес электронной почты.</p>
    <p>При регистрации вам было выслано письмо с ссылкой на подтверждение.</p>
    <p>Если по каким-то причинам, вы не получили письмо, вы можете отправить себе новый запрос на подтверждение</p>
  </div>
  <div class="panel-footer">
    <a href="/profile/verify-email" class="btn btn-block btn-warning">Отправить письмо для подтверждения</a>
  </div>
</div>