@if(config("app.env") == "production")
	<!-- ROISTAT BEGIN -->
	<script>
		(function(w, d, s, h, id) {
			w.roistatProjectId = id; w.roistatHost = h;
			var p = d.location.protocol == "https:" ? "https://" : "http://";
			var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
			var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
		})(window, document, 'script', 'cloud.roistat.com', 'f92266e7e09fbfe475f5d7aa71626916');
	</script>
	<!-- ROISTAT END -->

	<!-- YADRO BEGIN -->
	<script>
		(function(d, w, k) {
				w.introvert_callback = function() {
						try {
								w.II = new IntrovertIntegration(k);
						} catch (e) {console.log(e)}
				};
				var n = d.getElementsByTagName("script")[0],
						e = d.createElement("script");
				e.type = "text/javascript";
				e.async = true;
				e.src = "https://api.yadrocrm.ru/js/cache/"+ k +".js";
				n.parentNode.insertBefore(e, n);
		})(document, window, 'c8c22369');
	</script>
	<!-- YADRO END -->
@endif