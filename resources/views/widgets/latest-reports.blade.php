<div class="panel panel-default">
  <div class="panel-heading text-bold">Последние отчёты</div>
    @if(count($reports) == 0)
      <div class="panel-body">
        У вас еще нет актуальных отчётов.
      </div>
    @else
      <table class="table">
        <tbody>
          @foreach($reports as $report)
            <tr>
              <td class="text-center">
                @if($report->is_watched == 0) 
                  <span class="led led-primary led-pulsar" title="Не просмотренный отчёт" id="led-{{$report->id}}"></span> 
                @endif
              </td>
              <td>
                @if($report->file_name)
                  @if($report->is_watched == 0) 
                    <a href="{{ $report->getreportLink() }}" class="report-item text-bold" target="_blank" data-id="{{$report->id}}" title="Не просмотренный отчёт">Открыть отчёт</a>
                  @else
                    <a href="{{ $report->getreportLink() }}" class="report-item" target="_blank">Открыть отчёт</a>
                  @endif
                @else
                  <a href="{{ $report->getreportLink() }}" class="report-item">Актуализировать отчёт</a>
                @endif
              </td>
              <td>
                <div class="traffic-light">
                  @if($report->getStatement('green'))
                    <span class="led led-success led-lg"></span> 
                  @endif
                  @if($report->getStatement('yellow'))
                    <span class="led led-warning led-lg"></span> 
                  @endif
                  @if($report->getStatement('red'))
                    <span class="led led-danger led-lg"></span> 
                  @endif
                  @if(!$report->getStatement('green') && !$report->getStatement('yellow') && !$report->getStatement('red'))
                    <span class="led led-default led-lg"></span> 
                  @endif
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
      <div class="panel-footer">
        <a href="/reports" class="btn btn-default">Все отчёты</a>
      </div>
    @endif
</div>