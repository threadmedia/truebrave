@if (Session::has('new_message_text'))
  <div class="panel panel-{{ Session::get('new_message_state') }}">
    <div class="panel-heading text-bold">
      {!! Session::get('new_message_title') !!}
    </div>
    <div class="panel-body">
      {!! Session::get('new_message_text') !!}
    </div>
  </div>
@endif