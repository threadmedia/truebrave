		<div class="panel panel-default widget widget-tariff">
			<div class="panel-heading text-bold">Ваш тариф</div>
			<div class="panel-body">
				@if(Auth::user()->getTariff())
					<h3>{{ Auth::user()->getTariff()->title }}</h3>
					<p>Период действия: 
						@if(Auth::user()->getTariff()->period)
							{{Auth::user()->getTariff()->period}} мес.
							<p>Истекает через: {{Auth::user()->getCurrentOrder()->daysBeforeExpire() }}</p>
						@else
							<span class="ico ico-infinite" title="Без ограничений"></span>
						@endif
					</p>
					
					<p>Число запросов: 
						@if(Auth::user()->getTariff()->request_count) 
							{{Auth::user()->getTariff()->request_count}}
							<p>Оставшееся число запросов: {{Auth::user()->getRequestCount()}}</p>
						@else
							<span class="ico ico-infinite" title="Без ограничений"></span>
						@endif
					</p>

					@if(Auth::user()->getCurrentOrder()->isExpired())
            <div class="sticker sticker-expired text-center">
						  <strong>Действие вашего тарифа истекло.</strong>
            </div>
						<div class="chose-btn-wrapper">
							<a href="/tariffs/list" class="btn btn-ghost">Выбрать тариф</a>
						</div>
					@else
						<div class="chose-btn-wrapper">
							<a href="/reports/list" class="btn btn-ghost">Сформировать Экспресс-отчет</a>
						</div>
					@endif
				@else
					<p>Вы не выбрали тариф.</p>
					<div class="chose-btn-wrapper">
						<a href="/tariffs/list" class="btn btn-ghost">Выбрать тариф</a>
					</div>
				@endif
			</div>
		</div>