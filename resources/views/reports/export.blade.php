@php
	$bold    = "font-weight: bold; ";
	$center  = "text-align: center; ";
	$vcenter = "vertical-align: middle; ";
	$vbottom = "vertical-align: bottom; ";
	$font = '';
	$border = 'border: 1px #000000 solid; ';
  $fsize = 'font-size: 10; ';
  $fsize_header = 'font-size: 14; ';
	$vpadding = 'padding: 0 4px 0 4px; ';
  $text		  = '';
@endphp

<style>
  tr > td {border: 1px solid #000000;}
</style>

{{-- <html> --}}
<meta charset="UTF-8" />
<html lang="ru">
<body>
  <table border='1' style="border-collapse: collapse; font-size: 10px; font-family: Arial">
    <thead>
      <tr>
        <th style="{{$fsize}} {{$center}}">№ Заказа</th>
        <th style="{{$fsize}} {{$center}}">ФИО</th>
        <th style="{{$fsize}} {{$center}}">Email</th>
        <th style="{{$fsize}} {{$center}}">Территория</th>
        <th style="{{$fsize}} {{$center}}">Тип пакета</th>
        <th style="{{$fsize}} {{$center}}">Футболка</th>
        <th style="{{$fsize}} {{$center}}">Дата регистрации</th>
      </tr>
    </thead>
    <tbody>
      @foreach($user_list AS $user)
        <tr>
          <td style="{{$bold}}">
            @if(count($user->packages) > 0)
              c{{$user->id}}-t2-p{{ $user->packages[0]->id }}
            @endif
          </td>
          <td style="{{$fsize}}">
            {{ $user->last_name }} {{ $user->first_name }} {{ $user->patronymic }}
          </td>
          <td style="{{$fsize}}">
            {{ $user->email }}
          </td>
          <td style="{{$fsize}}">
            @if($user->territory)
              {{ $user->territory->title }}
            @endif
          </td>
          <td style="{{$fsize}}">
            @if(count($user->package_types) > 0)
              {{ $user->package_types[0]->title }}
            @endif
          </td>
          <td style="{{$fsize}}">
            @if(count($user->packages) > 0)
              {{ $user->packages[0]->title }}
            @endif
          </td>
          <td style="{{$fsize}}">
            {{ $user->created_at }}
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>