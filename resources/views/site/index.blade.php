@extends('layouts.app')
@section('content')


<section class="section-promo">
	<div class="header__logo">
		<img src="{{ config('app.url') }}/img/logo.svg" alt="Logo">
	</div>

	<h1 class="header__title">7 сентября</h1>
	<div class="header__desc">ханты-мансийск,
		<span>территория гостиницы «Олимпийская»</span>
	</div>
</section>

<section class="section section-fond">
	<div class="container">
		<h2 class="section-title">Призовой фонд</h2>

		<div class="fond">
			<span class="amount">{{(50000+$payment_list->sum('amount'))/1000}} 000</span> <span class="total">/ 450 000</span>
		</div>
		<div class="details">
			<p>Регистрация каждой команды увеличивает призовой фонд на 2000 рублей<br>
			Больше команд - больше призовой фонд</p>
			{{-- <p>Всего зарегистрировано команд: <span class="team-count">{{$team_list->count()}}/200</span></p> --}}
		</div>
	</div>
</section>

<section class="about" id="about">
	<div class="container">
		<h2 class="title about__title">О забеге</h2>
		{{-- <p>Ханты-Мансийск, 7 сентября, 1000 участников, 4 километра, 26 препятствий.</p>
		<p>Участниками забега могут стать югорчане от 18 до 60 лет. В составе команды должно быть пять человек, из которых минимум одна девушка. На пути каждой команды встанет 26 естественных и искусственных преград, которые растянутся на 4 километра.</p>
		<p>«Дело храбрых» – для тех, кому любопытно, страшно, легко, невыносимо. Для тех, кто хочет проверить, убедиться, доказать, гордиться.</p> --}}
		
		<p>Тысяча югорчан сможет испытать свои силы и выносливость в начале этой осени: 7
сентября в Ханты-Мансийске развернется трасса экстремального забега «Дело храбрых».
Регистрация команд открыта на сайте делохрабрых.рф до 6 сентября 12.00 часов.
Участниками забега могут стать югорчане в возрасте от 18 до 60 лет, в составе
команды – пять человек, минимум одна из которых девушка. Местом для возведения
препятствий и проведения забега станет территория в 200 тысяч квадратных метров за
гостиницей «Олимпийская». На пути каждой команды встанет 26 преград, которые
растянутся на 4 километра. Храбрецам придется преодолевать естественные и
искусственные преграды: элементы общевойсковой полосы препятствий и участки
пересеченной местности, водоемы и ограждения, бастионы и этапы с весами.
Экстремальные забеги не в новинку для югорчан и многие уже знают, чего ожидать.
Но забег забегу рознь: «Дело храбрых» удивит искушенных – и не очень – участников
масштабом и разнообразием препятствий и спецэффектов на трассе. А для гостей и
зрителей мероприятия будет организована насыщенная и интересная развлекательная
программа.</p>

<p>Мероприятие нацелено привлечь внимание югорчан к комплексу ГТО, здоровому образу
жизни, а также на создание условий, способствующих формированию у жителей региона
желания заниматься активными видами спорта.</p>

<p>Проект реализуется с использованиием гранта Губернатора Ханты-Мансийского автономного округа - Югры на развитие гражданского общества предоставленного Фондом "Центр гражданских и социальных инициатив Югры".</p>
		
		
		<div class="about-wrap">
			<div class="about__photo"><img src="img/photo/event-photo-1.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-2.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-3.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-4.jpg" alt="Photo event"></div>
		</div>

		<a href="{{ config('app.url') }}/docs/regulations.pdf" class="btn btn-form" target="blank"><span>Скачать положение</span></a>
	</div>
</section>

@guest
<!-- registratin form -->
<section class="registration" id="registration">
	<div class="container">
		@php
			$second = Carbon\Carbon::create(2019, 9, 6, 15, 00, 00);
			$first  = Carbon\Carbon::now();
		@endphp
		@if ($first->lessThan($second))
		<h2 class="title registration__title">Регистрация</h2>
		{{-- <p>Чтобы участвовать в забеге нужно пройти обязательную регистрацию. Участие в мероприятии платное.</p> --}}
			<p>Дата и время окончания регистрации <span>6 сентября - 12:00</span></p>
			<form class="form" id="signup-form">
				<h3 class="form__title">Команда</h3>

				<div class="form-couple">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 text-right">
							<span class="form__input-desc">Название команды</span>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pl-2 pr-0 validate-block">
							<input type="text" class="form__input" id="team_name" name="team_name" required>
						</div>
					</div>
				</div>

				<div class="form-couple">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 text-right">
							<span class="form__input-desc">Город</span>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pl-2 pr-0 validate-block">
							<input type="text" class="form__input" id="city" name="city" required>
						</div>
					</div>
				</div>

				<h3 class="form__title">Капитан команды</h3>

				<div class="form-couple">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 text-right">
							<span class="form__input-desc">ФИО / Дата рождения</span>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 pl-2 pr-0 validate-block">
							<input type="text" class="form__input" id="cap_fio" name="cap_fio" required placeholder="ФИО">
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 pl-2 pr-0 validate-block">
							<input type="text" class="form__input datepicker" id="birthdate" name="birthdate" data-toggle="datepicker" required placeholder="Дата рождения">
						</div>
					</div>
				</div>

				<div class="form-couple">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 text-right">
							<span class="form__input-desc">Email</span>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-8 pl-2 pr-0 validate-block">
							<input type="text" class="form__input" id="email" name="email" required>
						</div>
					</div>
				</div>

				<div class="form-couple">
					<div class="row">
						<div class="col-xs-12 col-sm-4 col-md-4 text-right">
							<span class="form__input-desc">Телефон</span>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-8 pl-2 pr-0 validate-block">
							<input type="text" class="form__input" id="phone" name="phone" required placeholder="8__________">
						</div>
					</div>
				</div>

				<h3 class="form__title">Члены команды</h3>
				<span class="form__title-warning">
					ВНИМАНИЕ! В составе команды должна быть минимум 1 (одна) девушка. 
					{{-- Команда, проигнорировавшая данное условие, будет дисквалифицирована. --}}
				</span>


				@for ($i = 1; $i <= 4; $i++)
					<div class="form-couple">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 text-right">
								<span class="form__input-desc">Участник №{{ $i+1 }}</span>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 pl-2 pr-0 validate-block">
								<input type="text" class="form__input" id="fio{{ $i }}" name="fio_{{ $i }}" required placeholder="ФИО">
							</div>
							<div class="col-xs-12 col-sm-6 col-md-4 pl-2 pr-0 validate-block">
								<input type="text" class="form__input datepicker" id="birthdate{{ $i }}" name="birthdate_{{ $i }}" data-toggle="datepicker" required placeholder="Дата рождения">
							</div>
						</div>
					</div>
				@endfor

				<label for="is_older_18" class="form__label validate-block">
					{{ Form::checkbox('is_older_18', 1, false, ['class' => 'form__checkbox', 'id' => 'is_older_18']) }}
					<div class="form__checkbox-fake"></div>
					<span>Я подтверждаю, что на день проведения мероприятия в составе команды не будет лиц, не достигших возраста 18 лет.</span>
				</label>

				<label for="is_pdn_agree" class="form__label validate-block">
					{{ Form::checkbox('is_pdn_agree', 1, false, ['class' => 'form__checkbox', 'id' => 'is_pdn_agree']) }}
					<div class="form__checkbox-fake"></div>
					<span>Я ознакомился и согласен с <a href="{{ config('app.url') }}/docs/privacy_policy.pdf" target="blank">политикой обработки персональных данных</a> (152-ФЗ)</span>
				</label>

				<label for="is_team_pdn_agree" class="form__label validate-block">
					{{ Form::checkbox('is_team_pdn_agree', 1, false,  ['class' => 'form__checkbox', 'id' => 'is_team_pdn_agree']) }}
					<div class="form__checkbox-fake"></div>
					<span>Я подтверждаю, что получил согласие членов команды на использование и передачу их персональных данных (152-ФЗ)</span>
				</label>

				<div class="form__participation-title">Стоимость регистрации команды</div>
				<span class="form__participation-value">{{ config('app.price') }} руб</span>
				
				<div id="error-block" class="alert alert-warning error-block hidden">
					Упс! Вы допустили ошибку.
				</div>

				<button type="submit" id="signup" class="btn btn-form">
					<span>ДАЛЕЕ</span>
					<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
				</button>
			</form>

		@else
			<h2 class="title registration__title">Регистрация завершена</h2>
		@endif

	</div>
</section>
@endguest

<section class="event-program" id="event-program">
	<div class="container">
		<h2 class="title event-program__title">Программа мероприятия</h2>
		<table class="event-program__table">
			<tr class="event-program__table-row">
				<td class="event-program__time">10:00</td>
				<td class="event-program__action">Сбор команд-участниц. Прохождение мандатной комиссии. Переодевание, сдача вещей в камеру хранения. Получение чипов.</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:00</td>
				<td class="event-program__action">Показательные выступления</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:30</td>
				<td class="event-program__action">Открытие и приветственное слово от приглашенных гостей.</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:45</td>
				<td class="event-program__action">Массовая разминка</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">13:00</td>
				<td class="event-program__action">Старт экстремального забега</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">14:30</td>
				<td class="event-program__action">Финиш экстремального забега</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">15:00</td>
				<td class="event-program__action">Торжественное подведение итогов, награждение победителей и призеров забега. Церемония закрытия</td>
			</tr>
		</table>
	</div>
</section>

<section class="event-place" id="event-place">
	<div class="container">
		<h2 class="title event-place__title">Место проведения</h2>
		<p>Трасса экстремального забега будет расположена на территории более 200 тысяч квадратных метров за гостиницей «Олимпийская», по адресу ул. Энгельса 45.</p>
		<div class="event-place__img"><img src="img/photo/event-place.jpg" alt="Event place"></div>
	</div>
</section>

<span id="signin-section-anchor"></span>
<section class="signin-section hidden" id="signin-section">
	<div class="container">
		<h2 class="title registration__title signin__title">Авторизация</h2>

		<form class="form" id="signin-form">
			<p class="font-roboto">Введите ваш email, который вы указывали при регистрации. Вам будет выслана ссылка для авторизации.</p>
			<br><br>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12">
						<span class="form__input-desc text-center">Email</span>
					</div>
					<div class="col-xs-12">
						<input type="email" class="form__input" name="email" required>
					</div>
				</div>
			</div>

			<div id="error-block" class="alert alert-warning error-block hidden">
				Указанный email отсутствует в системе
			</div>

			<button type="submit" id="signin" class="btn btn-form">
				<span>ДАЛЕЕ</span>
				<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
			</button>
		</form>
	</div>
</section>

@endsection