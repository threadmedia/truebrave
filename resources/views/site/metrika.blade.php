@extends('layouts.app')
@section('content')

<div class="container-fluid metrika">
	<div class="row">

    @php
      $package_sizes = App\Package::get();
    @endphp

    @foreach($territory_list as $territory)
		<div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <strong>{{ $territory->title }}</strong>
        </div>

        @php
          $users = App\User::where('territory_id', $territory->id)->get();
          $userIDs = $users->unique()->pluck('id');
          $package_simple   = App\PackageUser::whereIn('user_id', $userIDs)->where('package_type_id', 1)->get();
          $package_complex  = App\PackageUser::whereIn('user_id', $userIDs)->where('package_type_id', 2)->get();

          $package_complex_free   = App\PackageUser::whereIn('user_id', $userIDs)->where('package_type_id', 2)->whereNull('payment_at')->get();
          $package_complex_payed  = App\PackageUser::whereIn('user_id', $userIDs)->where('package_type_id', 2)->whereNotNull('payment_at')->get();
          $package_free = App\EventTerritory::where('territory_id', $territory->id)->first();
          $package_distribution = App\PackageDistribution::where('territory_id', $territory->id)->get();
        @endphp

        <table class="table table-bordered table-condensed table-stripe">
          <tbody>
            <tr>
              <td>Общее число зарегистрировавшихся:</td>
              <td class="text-center" width="150px">{{ $users->count() }}</td>
            </tr>
            <tr>
              <td>Количество бесплатных регистраций (только значок):</td>
              <td class="text-center" width="150px">{{ count($package_simple) }}</td>
            </tr>
            <tr>
              <td>Количество регистраций (платный сувенирный пакет): </td>
              <td class="text-center" width="150px">
                <span class="label label-success">{{ count($package_complex) }}</span>
                <span class="label label-default" title="Подаренные">{{ count($package_complex_free) }}</span>
                <span class="label label-primary" title="Оплаченные">{{ count($package_complex_payed) }}</span>
              </td>
            </tr>
            <tr>
              <td>Количество оставшихся бесплатных сувенирных пакетов:</td>
              <td class="text-center" width="150px">{{ $package_free->amount }}</td>
            </tr>
            <tr>
              <td >Количество оставшихся футболок по размерам:</td>
              <td class="text-center" width="150px">
                <span class="label label-success">{{ $package_distribution->sum('amount') }}</span>
              </td>
            </tr>
            @foreach($package_sizes as $package_size)
              <tr>
                <td class="text-center">{{ $package_size->title }}:</td>
                <td class="text-center" width="150px">{{ ($package_distribution->where('package_id', $package_size->id)->first()->amount) }}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div><!-- .panel -->
    </div><!-- .col-md-6 -->
    @endforeach


    @php
      $packages_free  = App\PackageUser::where('package_type_id', 2)->whereNull('payment_at')->get();
      $packages_payed = App\PackageUser::where('package_type_id', 2)->whereNotNull('payment_at')->get();
    @endphp

    <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Summary</strong>
        </div>
        <table class="table table-bordered table-condensed table-stripe">
          <tbody>
            <tr>
              <td>Всего реализовано футболок:</td>
              <td class="text-center" width="100px">{{ $packages_free->count() + $packages_payed->count() }}</td>
            </tr>
            <tr>
              <td>Из них подарено:</td>
              <td class="text-center" width="100px">{{ $packages_free->count() }}</td>
            </tr>
            <tr>
              <td>Из них продано:</td>
              <td class="text-center" width="100px">{{ $packages_payed->count() }}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>


  </div>
</div>

@endsection