
@extends('layouts.dash')
@section('content')

<!-- production-версия, оптимизированная для размера и скорости-->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Консоль</strong>
          </div>
          <div class="panel-body">
            <div class="row">

              <div class="col-md-3">
                <div class="form-group">
                  <label for="">Территории</label>
                  <select v-model="form.territory_id" class="form-control" placeholder="Территории">
                    <option value="0" >Все территории</option>
                    <option v-for="territory in form.territory_list" v-bind:key="territory.id" v-bind:value="territory.id" >@{{territory.title}}</option>
                  </select>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                  <label for="">Типы пакетов</label>
                  <select v-model="form.package_type_id" class="form-control" placeholder="Территории">
                    <option value="0">Все типы</option>
                    <option v-for="package_type in form.package_type_list" v-bind:key="package_type.id" v-bind:value="package_type.id" >@{{package_type.title}}</option>
                  </select>
                </div>
              </div>

              {{-- <div class="col-md-3">
                <div class="form-group">
                  <label for="">Размеры</label>
                  <select v-model="form.package_id" class="form-control" placeholder="Территории">
                    <option value="0">Все размеры</option>
                    <option v-for="package in form.package_list" v-bind:key="package.id" v-bind:value="package.id" >@{{package.title}}</option>
                  </select>
                </div>
              </div> --}}

              <div class="col-md-3">
                <div class="form-group">
                  <label for="">Email</label>
                  <input type="text" v-model="form.email" class="form-control" placeholder="Email">
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12 text-right">

                <div class="btn-group">
                    <button type="button" class="btn btn-primary" @click="search()">Поиск</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li><a href="#" @click="search('xls')">Экспорт в Excel</a></li>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="panel panel-default" v-if="user_list.length > 0">
          <div class="panel-heading">
            <span class="chunk bg-green">Всего пользователей: @{{user_list.length}}</span>
            <span class="chunk bg-green" v-if="form.package_type != 1">Сумма: @{{ total_sum() }}</span>
          </div>
          <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>ФИО</th>
                <th>Email</th>
                <th>Территория</th>
                <th>Тип пакет</th>
                <th>Тип футболки</th>
                <th>Дата регистрации</th>
                <th class="text-center">
                  <i class="glyphicon glyphicon-cog"></i>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="user in user_list" :key="user.id">
                <td>@{{user.id}}</td>
                <td>@{{user.last_name}} @{{user.first_name}} @{{user.patronymic}}</td>
                <td>@{{user.email}}</td>
                <td>
                  <template v-if="user.territory_id > 0">
                    @{{user.territory.title}}</td>
                  </template>
                <td>
                  <template v-if="user.package_types.length > 0">
                    @{{user.package_types[0].title }}
                  </template>
                </td>
                <td>
                  <template v-if="user.packages.length > 0">
                    <span class="color-green pull-right" v-if="user.package_types[0].pivot.payment_id != -1">
                      <i class="glyphicon glyphicon-usd"></i>
                    </span>
                    @{{user.packages[0].title }}
                    <div class="block--info">
                      <div class="chunk chunk-sm bg-yandex" v-if="user.package_types[0].id == 2">
                        c@{{user.id }}-t2-p@{{user.packages[0].id }}
                      </div>
                    </div>
                  </template>
                  <div>
                    № <strong>@{{ user.reg_number }}</strong>
                  </div>
                </td>
                <td>@{{user.created_at}}</td>
                <td class="text-center">
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                      <li><a href="#" @click="clear(user.id)">Сбросить пакет</a></li>
                      <li><a href="#" @click="edit(user.id)">Редактировать</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#" @click="delete(user.id)">Удалить</a></li>
                    </ul>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  new Vue({
  el: '#app',
  data: {
    loading: false,
    user_list: [],  
    form: {
      email: '',
      territory_id: null,
      territory_list: [],
      package_id: null,
      package_list: [],
      package_type_id: null,
      package_type_list: [],
    },
  },
  methods: {
    /**
    * Init
    */
    init: function () {
      let self = this;
      axios.get('/api/dash/init')
      .then(function (r) {
        self.form.territory_list = r.data.territory_list;
        self.form.package_list = r.data.package_list;
        self.form.package_type_list = r.data.package_type_list;
      })
      .catch(function (error) { console.log(error); });
    },

    total_sum() {
      let sum = 0;
      if(this.user_list.length > 0) {
        this.user_list.forEach(function (user) {
          // console.log(user);
          if(user.package_types.length > 0 && user.package_types[0].id == 2 && user.package_types[0].pivot.payment_id != -1) {
            sum = sum + 500;
          }
        });
      }
      return sum;
    },

    clear: function(user_id) {},
    edit: function(user_id) {},
    delete: function(user_id) {},

    /**
    * Search
    */
    search: function (mode = false) {
      let query_mode = (mode) ? mode : 'query';
      let self = this;
      let post_data = {
        'email': self.form.email, 
        'territory_id': self.form.territory_id, 
        'package_id': self.form.package_id, 
        'package_type_id': self.form.package_type_id,
        'mode': query_mode, 
      };
      let response_type = (query_mode != 'query') ? {'responseType': "blob"} : {'responseType': "json"};
      axios.post('/api/users/search', post_data, response_type)
      .then(function (r) {
        if(query_mode == 'query') {
          self.user_list = r.data.user_list;
        }
        else {
          self.saveFileFromResponse(r);
        }
      })
      .catch(function (error) { console.log(error); });
    },

    parseResponseHeaders: function (headerStr) {
      var headers = {};
      if (!headerStr) {
        return headers;
      }
      var headerPairs = headerStr.split('; ');
      for (var i = 0, len = headerPairs.length; i < len; i++) {
        var headerPair = headerPairs[i];
        var index = headerPair.indexOf('=');
        if (index > 0) {
          var key = headerPair.substring(0, index);
          var val = headerPair.substring(index + 1);
          if(key == 'filename*')
            val = val.substring(7);
          if(key == 'filename') 
            val = val.substring(1,val.length - 1);
          headers[key] = decodeURI(val);
        }
      }
      return headers;
    },

    save: function (blob, name) {
      name = name || 'download';
      if (typeof navigator !== "undefined") {
        if (/MSIE [1-9]\./.test(navigator.userAgent)) {
          alert('IE is unsupported before IE10');
          return;
        }
        if (navigator.msSaveOrOpenBlob) {
          navigator.msSaveOrOpenBlob(blob, name);
          return;
        }
      }
      var win_url = window.URL || window.webkitURL || window;
      var url = win_url.createObjectURL(blob);
      var a = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
      if ('download' in a) {
        a.href = url;
        a.download = name;
        a.dispatchEvent(new MouseEvent('click'));
        setTimeout(function() { win_url.revokeObjectURL(url); }, 500);
        return;
      }
      window.location.href = url;
      setTimeout(function() { win_url.revokeObjectURL(url); }, 500);
    },

    saveFileFromResponse: function (response) {
      let filename = this.parseResponseHeaders(response.headers['content-disposition']);
      filename = (filename['filename*']) ? filename['filename*'] : filename['filename'];
      let name = decodeURI(filename);
      this.save(response.data, name);
    },
    
  },
  mounted: function () {
    this.init();
  }
})
</script>

@endsection