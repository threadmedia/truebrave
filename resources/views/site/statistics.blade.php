@extends('layouts.app')
@section('content')

<section class="about" id="about">
	<div class="container">
		<h2 class="title about__title">О забеге</h2>
		<p>Ханты-Мансийск, 7 сентября, 1000 участников, 4,5 километра, 26 препятствий.</p>
		<p>Участниками забега могут стать югорчане от 18 до 60 лет. В составе команды должно быть пять человек, из которых минимум одна девушка. На пути каждой команды встанет 26 естественных и искусственных преград, которые растянутся на 4,5 километра.</p>
		<p>«Дело храбрых» – для тех, кому любопытно, страшно, легко, невыносимо. Для тех, кто хочет проверить, убедиться, доказать, гордиться.</p>
		<div class="about-wrap">
			<div class="about__photo"><img src="img/photo/event-photo-1.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-2.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-3.jpg" alt="Photo event"></div>
			<div class="about__photo"><img src="img/photo/event-photo-4.jpg" alt="Photo event"></div>
		</div>
	</div>
</section>

<!-- registratin form -->
<section class="registration" id="registration">
	<div class="container">
		<h2 class="title registration__title">Регистрация</h2>
		@auth
			<form class="form" id="signup-form">
				<div class="form-couple">
					<span class="form__input-desc">Название команды</span>
				<input type="text" class="form__input" name="team_name" value="{{\Auth::user()->team_name}}" disabled>
				</div>
	
				<div class="form__participation-title">Взнос за участие команды</div>
				<span class="form__participation-value">2000 руб</span>
	
				<a href="{{ route('site.order')}}" type="submit"class="btn btn-form">
					<span>Оплатить</span>
				</a>
			</form>
		@endauth

		@guest
		<p>Чтобы участвовать в забеге нужно пройти обязательную регистрацию. Участие в мероприятии платное.</p>
		<p>Дата и время окончания регистрации <span>6 сентября - 12:00</span></p>
		<form class="form" id="signup-form">
			<h3 class="form__title">Команда</h3>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Название команды</span>
					</div>
					<div class="col-xs-12 col-md-8 pl-2 pr-0">
						<input type="text" class="form__input" name="team_name" required>
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Город</span>
					</div>
					<div class="col-xs-12 col-md-8 pl-2 pr-0">
						<input type="text" class="form__input" name="city" required>
					</div>
				</div>
			</div>

			<h3 class="form__title">Капитан команды</h3>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">ФИО / Дата рождения</span>
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="cap_fio" required placeholder="ФИО">
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="birthdate" data-toggle="datepicker" required placeholder="Дата рождения">
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Email</span>
					</div>
					<div class="col-xs-12 col-md-8 pl-2 pr-0">
						<input type="text" class="form__input" name="email" required>
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Телефон</span>
					</div>
					<div class="col-xs-12 col-md-8 pl-2 pr-0">
						<input type="text" class="form__input" name="phone" required placeholder="8__________">
					</div>
				</div>
			</div>

			<h3 class="form__title">Члены команды</h3>
			<span class="form__title-warning">
				ВНИМАНИЕ! В составе команды должна быть минимум 1 (одна) девушка. Команда, проигнорировавшая данное условие, будет дисквалифицирована.
			</span>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Участник №2</span>
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="teamate[1]" required placeholder="ФИО">
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="birthdate[1]" data-toggle="datepicker" required placeholder="Дата рождения">
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Участник №3</span>
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="teamate[2]" required placeholder="ФИО">
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="birthdate[2]" data-toggle="datepicker" required placeholder="Дата рождения">
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Участник №4</span>
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="teamate[3]" required placeholder="ФИО">
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="birthdate[3]" data-toggle="datepicker" required placeholder="Дата рождения">
					</div>
				</div>
			</div>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Участник №5</span>
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="teamate[4]" required placeholder="ФИО">
					</div>
					<div class="col-xs-12 col-md-4 pl-2 pr-0">
						<input type="text" class="form__input" name="birthdate[4]" data-toggle="datepicker" required placeholder="Дата рождения">
					</div>
				</div>
			</div>

			<label for="agree-1" class="form__label">
				{{ Form::checkbox('is_older_18', '1', false, ['class' => 'form__checkbox', 'id' => 'agree-1']) }}
				<div class="form__checkbox-fake"></div>
				<span>Я подтверждаю, что на день проведения мероприятия в составе команды не будет лиц, не достигших возраста 18 лет.</span>
			</label>

			<label for="agree-2" class="form__label">
				{{ Form::checkbox('is_pdn_agree', '1', false, ['class' => 'form__checkbox', 'id' => 'agree-2']) }}
				<div class="form__checkbox-fake"></div>
				<span>Я ознакомился и согласен с политикой обработки персональных данных (152-ФЗ)</span>
			</label>

			<label for="agree-3" class="form__label">
				{{ Form::checkbox('is_team_pdn_agree', '1', false, ['class' => 'form__checkbox', 'id' => 'agree-3']) }}
				<div class="form__checkbox-fake"></div>
				<span>Я подтверждаю, что получил согласие членов команды на использование и передачу их персональных данных (152-ФЗ)</span>
			</label>

			<div class="form__participation-title">Взнос за участие команды</div>
			<span class="form__participation-value">2000 руб</span>
			
			<div id="error-block" class="alert alert-warning error-block hidden">
				Тут где-то ошибка
			</div>

			<button type="submit" id="signup" class="btn btn-form">
				<span>ДАЛЕЕ</span>
				<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
			</button>
		</form>
		@endguest
	</div>
</section>

<section class="event-program" id="event-program">
	<div class="container">
		<h2 class="title event-program__title">Программа мероприятия</h2>
		<table class="event-program__table">
			<tr class="event-program__table-row">
				<td class="event-program__time">9:00</td>
				<td class="event-program__action">Начало работы площадки</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">10:00</td>
				<td class="event-program__action">Начало выдачи номеров и стартовых пакетов, начало работы раздевалок</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:00</td>
				<td class="event-program__action">Старт культурно-досуговой программы. Показательное выступление специальных служб, начало работы миллитари фотозон, начало работы фудкорта</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:15</td>
				<td class="event-program__action">Просмотр трассы</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">12:30</td>
				<td class="event-program__action">Торжественное открытие мероприятия</td>
			</tr>
			<tr class="event-program__table-row">
				<td class="event-program__time">13:00</td>
				<td class="event-program__action">Старт командного забега, старт корпоративного забега, старт индивидуального забега</td>
			</tr>
		</table>
	</div>
</section>

<section class="event-place" id="event-place">
	<div class="container">
		<h2 class="title event-place__title">Место проведения</h2>
		<p>Трасса экстремального забега будет расположена на территории более 200 тысяч квадратных метров за гостиницей «Олимпийская», по адресу ул. Энгельса 45.</p>
		<div class="event-place__img"><img src="img/photo/event-place.jpg" alt="Event place"></div>
	</div>
</section>

<span id="signin-section-anchor"></span>
<section class="signin-section hidden" id="signin-section">
	<div class="container">
		<h2 class="title registration__title signin__title">Авторизация</h2>

		<form class="form" id="signin-form">
			<p class="font-roboto">Введите ваш email, который вы указывали при регистрации. Вам будет выслана ссылка для авторизации.</p>
			<br><br>

			<div class="form-couple">
				<div class="row">
					<div class="col-xs-12 col-md-4 text-right">
						<span class="form__input-desc">Email</span>
					</div>
					<div class="col-xs-12 col-md-8 pl-2 pr-0">
						<input type="email" class="form__input" name="email" required>
					</div>
				</div>
			</div>

			<div id="error-block" class="alert alert-warning error-block hidden">
				Указанный email отсутствует в системе
			</div>

			<button type="submit" id="signin" class="btn btn-form">
				<span>ДАЛЕЕ</span>
				<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
			</button>
		</form>
	</div>
</section>

@endsection