@extends('layouts.app')
@section('content')

<!-- registratin form -->
<section class="payment" id="payment">
	<div class="container">
		<h2 class="title registration__title">Оплата</h2>
		
		<form class="form pl-4 pr-4" id="payment-form">
			<div class="text-center">
				<span class="form__input-desc text-center">Название команды</span>
				<h2>{{\Auth::user()->team_name}}</h2>
			</div>

			<div>
				<div @if(count($payment_list) > 0) class="hidden" @endif id="pay-block">
					<div class="form__participation-title">Стоимость услуги</div>
					<span class="form__participation-value">{{ config('app.price') }} руб</span>
					<button role="button" data-href="{{ route('payment.pay')}}" class="btn btn-form" id="pay">
						<span>Оплатить</span>
						<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
					</button>
				</div>
			</div>

			<div id="unique-code" class="unique-code hidden">
				<p>Вы успешно завершили регистрацию! <br>Код вашего участия:</p><br>
				<div id="scrapycoco">
					<img src="{{ config('app.url') }}/img/ajax-loader2.svg">
				</div>
				<div class="text-center pt-4">
					<p class="pb-2">Бланки, которые необходимо заполнить и передать организаторам в день мероприятия:</p>

					<div>
						<a role="button" class="btn btn-app" href="{{ config('app.url') }}/docs/disclaimer.pdf" target="_blank">
							<span>Бланк заявления отказа от претензий <br>(заполняется каждым участником команды)</span>
						</a>
					</div>
					<div>
						<a role="button" class="btn btn-app" href="{{ config('app.url') }}/docs/assignment.pdf" target="_blank">
							<span>Бланк заявления о передаче права участия <br>(заполняется при необходимости)</span>
						</a>
					</div>
					<hr>
					<p class="pb-2">Мы подготовили для вас ряд советов, которые помогут вам лучше подготовиться к мероприятию:</p>
					<div>
						<a role="button" class="btn btn-app" href="{{ config('app.url') }}/docs/race_guide.pdf" target="_blank">
							<span>Краткое руководство. Готовимся к забегу</span>
						</a>
					</div>
				</div>
			</div>
		</form>

		@if(count($payment_list) > 0)
			<div class="form payment-history">
				<table class="table" id="payment-table">
					<thead>
						<tr>
							<th width="40px">№</th>
							<th width="120px">Сумма</th>
							<th width="160px">Дата/время</th>
							<th>Статус</th>
						</tr>
					</thead>
					<tbody id="payment-table-body">
					</tbody>
				</table>
			</div>
		@endif

	</div>
</section>

@endsection