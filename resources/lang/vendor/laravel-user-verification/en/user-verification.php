<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel User Verification Language Pack
    |--------------------------------------------------------------------------
    */

    'verification_email_subject' => 'Подтверждение аккаунта',

    // error view
    'verification_error_header' => 'Ошибка подтверждения',
    'verification_error_message' => 'Ваш аккаунт не подтвержден.',
    'verification_error_back_button' => 'Назад',

];
