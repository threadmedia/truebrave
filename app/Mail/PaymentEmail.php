<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class PaymentEmail extends Mailable
{
	use Queueable, SerializesModels;

	public $user;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(User $user, $order_number)
	{
		$this->user = $user;
		$this->order_number = $order_number;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$this->subject('Регистрация завершена')->view('emails.registration-complete');
		if($this->user->territory->eventTerritory->is_disabled != 1) {
			$this->attach('tickets/' . $this->user->id . '-ticket.pdf');
		}
		$this->with(['user' => $this->user, 'order_number' => $this->order_number,]);
		return $this;
	}
}
