<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sms extends Model
{
	use SoftDeletes;
	protected $table = 'sms';
	protected $primaryKey = 'id';


	/**
	 * Отправка сообщения
	 *
	 * @return object
	 */
	public static function sendMessage()
	{
		return $this->belongsTo(Tariff::class, 'tar_id', 'tar_id');
	}
}
