<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teamate extends Model
{
  protected $table = 'teamate';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 
		'fio', 
		'birthdate', 
	];


	public static function creation_rules() 
	{
		return [
			// 'user_id' 	   => 'required|numeric',
			'fio' => 'required|string|min:6',
			'birthdate' => 'required|date_format:d.m.Y',
		];
	}

	public static function updation_rules() {
		return [
			'fio' => 'required|string|min:6',
			'birthdate' => 'required|date_format:Y-m-d',
		];
	}

	/**
	 * User
	 *
	 * @return object
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}
}