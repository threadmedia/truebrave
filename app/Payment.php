<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
  protected $table = 'payment';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'user_id', 
		'product_id', 
		'pay_id', 
		'amount', 
		'approved_at', 
	];


	public static function creation_rules() 
	{
		return [];
	}


	/**
	 * User
	 *
	 * @return object
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}


	/**
	 * Only succeeded
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeSucceeded($query)
	{
		return $query->where('status', 'succeeded');
	}
}