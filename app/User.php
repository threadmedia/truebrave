<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable 
{
	// use SoftDeletes;
	use Notifiable;

  protected $table = 'user';
	protected $primaryKey = 'id';

  protected $appends = [
    'code',
  ];
	
	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'email', 
		'password', 
		'phone',
		'cap_fio',
		'city',
		'team_name',
		'birthdate'
	];

	public static function creation_rules() 
	{
		return [
			'role'      => 'required',
			'email'     => 'required|email|unique:user,email',
			'phone'     => 'required||digits:11',
			// 'password'  => 'required|string|min:6|confirmed',
			
			'cap_fio'   => 'required|string|min:6',
			'birthdate' => 'required|date_format:d.m.Y',

			'city'  	  => 'required|string|min:3',
			'team_name' => 'required|string|min:3|unique:user,team_name',
			
			'is_older_18'		=> 'required|boolean|accepted',
      'is_pdn_agree'	=> 'required|boolean|accepted',
      'is_team_pdn_agree'	=> 'required|boolean|accepted',
		];
	}

	public static function updation_rules() {
		return [
			'email'     => 'required|email',
			'phone'     => 'required||digits:11',
			'cap_fio'   => 'required|string|min:6',
			'birthdate' => 'required|date_format:Y-m-d',
			'city'  	=> 'required|string|min:3',
			'team_name' => 'required|string|min:3',
			
		];
	}

	// protected $appends = [
  //   'auth_token',
  // ];

	public function role_list() 
	{
		return [
			'user' => 'Пользователь',
			'manager' => 'Менеджер',
			'tester' => 'Тестировщик',
			'admin' => 'Администратор'
		];
	}


	public function getRole()
	{
		foreach($this->role_list() as $role => $title) {
			if($role == $this->role)
				return $title;
		}
		return false;
	}



	public function getCodeAttribute () {
		return  $this->id . '-' . str_replace(':', '', substr($this->created_at, -5)) . 'XTR';
	}

	/**
	 * Является ли пользователь администратором
	 */
	public function isAdmin() 
	{
		return ($this->role == 'admin') ? true : false;
	}


	/**
	 * Является ли пользователь клиентом
	 */
	public function isClient() 
	{
		return ($this->role == 'user') ? true : false;
	}


  /**
   * Токен авторизации
	 * 
	 * @return string
   */
	public function generate_token()
	{
    return md5($this->email.'86'.$this->id);
  }

	
	/**
	 * Teamates
	 *
	 * @return object
	 */
	public function teamates()
	{
		return $this->hasMany(Teamate::class, 'user_id', 'id');
	}


	/**
	 * Payments
	 *
	 * @return object
	 */
	public function payments()
	{
		return $this->hasMany(Payment::class, 'user_id', 'id');
	}

	/**
	 * Only published
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeAthletes($query)
	{
		return $query->where('role', 'user');
	}

	/**
	 * Only registred
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeRegistred($query)
	{
		return $query->whereNotNull('team_number');
	}
}