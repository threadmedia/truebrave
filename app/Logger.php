<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logger extends Model
{
  protected $table = 'logger';
	protected $primaryKey = 'id';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'request', 
		'result', 
	];
}