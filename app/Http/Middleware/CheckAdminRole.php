<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckAdminRole
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if(is_numeric(Auth::id()) && Auth::user()->role == 'admin') {
      return $next($request);
    }
    else
      return redirect()->route('dashboard.index');
  }
}
