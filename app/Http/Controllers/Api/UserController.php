<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Mail\PaymentComplete;
use App\Mail\TeamRegistred;
use App\Mail\EventPrerollEmail;

use App\{
  User,
  Payment,
  Logger
};


class UserController extends Controller
{
	/**
	 * list
	 */
	public function list(Request $request)
	{
    $user_list = User::with('teamates', 'payments')->athletes()->get();
    return compact('user_list');
  }

  /**
   * update user & teamete
  */  
  public function update(Request $request, $id) {
    $result  = 'error';
    $errors  = [];
    $message = 'Неизвестный тип при запросе';
    
    if ($request->exists('type')) {
        $type = $request->input('type');
        $input = $request->all();
        if ($type === 'user') {
            $input['role'] = 'user';
            $user_validator = Validator::make($input, User::updation_rules());
            if ($user_validator->fails()) {
                $errors = array_merge($errors, $user_validator->errors()->toArray());
            }
            if(count($errors) == 0) {
                $user = User::find($id);
                $user->update([
                    'cap_fio' => $input['cap_fio'],
                    'city' => $input['city'],
                    'birthdate' => $input['birthdate'],
                    'email' => $input['email'],
                    'phone' => $input['phone'],
                    'team_name' => $input['team_name']
                ]);
                return response()->json(['result' => 'success'], 200); 
            }
        } elseif ($type === 'teamate') {
            $teamate_validator = Validator::make($input, Teamate::updation_rules());
            if ($teamate_validator->fails()) {
              $errors = array_merge($errors, $teamate_validator->errors()->toArray());
            }
            if(count($errors) == 0) {
                $teamate = Teamate::find($id);
                $teamate->update([
                    'fio' => $input['fio'], 
                    'birthdate' => $input['birthdate']
                ]);
                return response()->json(['result' => 'success'], 200);
            }
        }
    }
    return compact('result', 'errors', 'message');
}    

	/**
	 * Send Confirm message
	 */
	public function confirm(Request $request, $id)
	{
    $user = User::findOrFail($id);
    Mail::to($user->email)->send( new PaymentComplete($user) );
    return response()->json(['result' => 'success'], 200);
  }


  /**
	 * Send Preroll message
	 */
	public function prerollTest(Request $request)
	{
    $id = 1;
    $user = User::findOrFail($id);
    Mail::to($user->email)->send( new EventPrerollEmail($user) );

    $id = 3;
    $user = User::findOrFail($id);
    Mail::to($user->email)->send( new EventPrerollEmail($user) );
    return response()->json(['result' => 'success'], 200);
  }


	/**
	 * Send Preroll message
	 */
	public function preroll(Request $request, $id)
	{
    $user = User::findOrFail($id);
    Mail::to($user->email)->send( new EventPrerollEmail($user) );
    return response()->json(['result' => 'success'], 200);
  }


  /**
	 * Mass sending
	 */
	public function mass_preroll(Request $request)
	{
    set_time_limit(1200);
    $user_list = User::where('role', 'user')->whereNotNull('team_number')->get();
    foreach ($user_list as $user) {
      sleep ( 1 );
      Mail::to($user->email)->send( new EventPrerollEmail($user) );
    }
    return response()->json(['result' => 'success'], 200);
  }
}