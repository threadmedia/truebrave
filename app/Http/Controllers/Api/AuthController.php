<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Mail\TeamRegistred;

use Carbon\Carbon;

use App\{
  User,
  Teamate
};


class AuthController extends Controller
{
  /**
   * Signup
   */
  public function signup(Request $request)
  {
    
    $result  = 'error';
    $errors  = [];
    $message = 'Не все поля заполнены верно';

    if ($request->exists('ticket')) {
      $input = $request->get('ticket');
      $input['role'] = 'user';

      $user_validator = Validator::make($input, User::creation_rules());
      if ($user_validator->fails())
        $errors = array_merge($errors, $user_validator->errors()->toArray());


      for($i = 1; $i <=4; $i++) {
        // dump($request->get('ticket'));
        $teamate[$i] = [
          'fio' => (isset($input['fio_'.$i])) ? $input['fio_'.$i] : null, 
          'birthdate' => (isset($input['birthdate_'.$i])) ? $input['birthdate_'.$i] : null,
        ];

        // dd($teamate[$i]);
        $teamate_validator = Validator::make($teamate[$i], Teamate::creation_rules());
        if ($teamate_validator->fails()) {
          $errors[$i] = $teamate_validator->errors()->toArray();
        }
        // $errors = array_merge($errors, $teamate_validator->errors()->toArray());
      }

      if(count($errors) == 0) {
        $result  = 'success';
        $message = 'Проверьте почту';
        $input['birthdate'] = Carbon::createFromFormat('d.m.Y', $input['birthdate'])->format('Y-m-d');
        
        $user = User::create($input);
        $user->auth_token = $user->generate_token();
        $user->save();

        for($i = 1; $i <=4; $i++) {
          $teamate[$i]['user_id'] = $user->id;
          $teamate[$i]['birthdate'] = Carbon::createFromFormat('d.m.Y', $teamate[$i]['birthdate'])->format('Y-m-d');
          Teamate::create($teamate[$i]);
        }
        Mail::to($user->email)->send( new TeamRegistred($user) );
      }
    }
    return compact('result', 'errors', 'message');
  }


  /**
   * Send auth link
   */
  public function send_auth_link(Request $request)
  {
    $result = 'error';
    $errors = [];
    $message = 'Не все поля заполнены верно';

    if ($request->exists('email')) {
      $email = $request->get('email');
      $user = User::where('email', $email)->first();
      if ($user) {
        $result  = 'success';
        $message = 'Проверьте почту';
        Mail::to($user->email)->send( new TeamRegistred($user) );
      }
    }
    return compact('result', 'errors', 'message');
  }

}