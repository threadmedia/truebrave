<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;

use Excel;

use App\{
	User,
  Territory,
  EventTerritory,
	Package,
	PackageUser,
	PackageType,
	PackageDistribution,
	Stuff
};


class DashController extends Controller
{
  
  /**
   * Init
   */
  public function init(Request $request)
  {
    $territory_list    = Territory::whereIn('id', EventTerritory::where('event_id', 1)->get()->pluck('territory_id'))->get();
    $package_list      = Package::get();
    $package_type_list = PackageType::where('id', 1)->get();
    return compact('territory_list', 'package_list', 'package_type_list');
  }



  /**
   * Search user
   */
  public function search(Request $request)
  {
    $user = new User();
    $user = $user->with('territory', 'packages', 'package_types')->whereNotNull('territory_id');
    if($request->exists('email') && !empty($request->get('email')) )
      $user = $user->where('email', 'LIKE', '%'.$request->get('email').'%');
    if($request->exists('territory_id') && is_numeric($request->get('territory_id')) ) {
      if($request->get('territory_id') > 0)
        $user = $user->where('territory_id', $request->get('territory_id'));
    }

    if($request->exists('package_type_id') && is_numeric($request->get('package_type_id')) ) {
      if($request->get('package_type_id') > 0) {
        $user->whereHas('package_types', function ($q) use ($request) {
          $q->where('package_type.id', $request->get('package_type_id'));
        });
      }
    }

    if($request->exists('package_id') && is_numeric($request->get('package_id')) ) {
      if($request->get('package_id') > 0) {
        $user->whereHas('packages', function ($q) use ($request) {
          $q->where('package.id', $request->get('package_id'));
        });
      }
    }   

    $user_list = $user->get();

    $user_list = $user_list->each(function ($item, $key) {
      $item->reg_number = $item->territory->id .'-'. $item->id .'-'. str_replace(':', '', substr($item->created_at, -5)) .'р';
      return $item;
    });


    # Export to XLS
    if($request->exists('mode') && $request->get('mode') == 'xls') {
      $file_name = 'export_'.rand(0,99).rand(0,99).rand(0,99).rand(0,99).rand(0,99);
      Excel::create($file_name, function($excel) use ($user_list) {
        $excel->sheet('export', function($sheet) use ($user_list) {
          $sheet->loadView('reports.export', ['user_list' => $user_list]);
          $lastrow = 1000;
          $sheet->getStyle('A1:O'. $lastrow )->getAlignment()->setWrapText(true);
        });
      })->store('xlsx');
      return response()->download(storage_path('exports/' . $file_name . '.xlsx'), $file_name.'.xlsx')->deleteFileAfterSend(true);
    }
    return compact('user_list');
  }



  /**
   * Clear user package
   */
  public function clear(Request $request)
  {
    // return compact('module');
  }
}