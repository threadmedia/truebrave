<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use YandexCheckout\Client;
use YandexCheckout\Model\Notification\NotificationSucceeded;
use YandexCheckout\Model\Notification\NotificationWaitingForCapture;
use YandexCheckout\Model\NotificationEventType;

use App\Mail\PaymentComplete;

use App\{
  User,
  Payment,
  Logger
};


class PaymentController extends Controller
{
	/**
	 * Pay
	 */
	public function pay(Request $request)
	{
    $email_list = ['a.dugaev@yandex.ru', 'kao@tm-ss.ru', 'deprimehell@gmail.com'];
		$client = new Client();
		$client->setAuth(config('ya_kassa.shop_id'), config('ya_kassa.secret'));
    $idempotence_key = uniqid('', true).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9);

		$payment = Payment::create([
			'user_id'    => Auth::id(),
			'product_id' => 1,
			'pay_id'     => Auth::user()->auth_token,
			'amount'     => (in_array(Auth::user()->email, $email_list)) ? 1 : config('app.price'),
		]);

		$payment_data = [
			'amount' => [
				'value' 		=> $payment->amount,
				'currency' 	=> 'RUB',
			],
			'payment_method_data' => [
				'type' => 'bank_card',
			],
			'confirmation' => [
				'type' 				=> 'redirect',
				'return_url' 	=> route('site.order'),
			],
			'description' => 'Заказ №'.$payment->id,
		];

		# Создаем платеж
		$response = $client->createPayment(
			$payment_data,
			$idempotence_key
		);
		$payment->pay_id = $response->id;
    $payment->save(); 

    $confirmation_url = $response->confirmation->confirmationUrl;
    return compact('confirmation_url');
    // return redirect()->away($response->confirmation->confirmation_url);
  }
  
  
  /**
   * Check payment status
   */
  public function check(Request $request, $payment_id = false)
  {
    // $payment_list = Payment::where('user_id', Auth::id())->whereNull('approved_at')->get();
    $unique_code  = 0;
    $tpl = Auth::user()->id . '-' . str_replace(':', '', substr(Auth::user()->created_at, -5)) . 'XTR';
    $payment_list = Payment::where('user_id', Auth::id())->get();
    
    $payment_succeed = Payment::where('user_id', Auth::id())->where('status', 'succeeded')->first();
    $unique_code = ($payment_succeed) ? $tpl : $unique_code;

    if (count($payment_list) > 0) {
      $client = new Client();
      $client->setAuth(config('ya_kassa.shop_id'), config('ya_kassa.secret'));

      foreach ($payment_list as $key => $item) {
        # Обрабатываем только не подтвержденные платежи
        if ($item->approved_at === null) {
          $payment = $client->getPaymentInfo($item->pay_id);
          if ($payment->status === "waiting_for_capture") {
            $idempotence_key = uniqid('', true).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
            # Списываем платеж
            $response = $client->capturePayment(
              ['amount' => ['value' => $item->amount, 'currency' => 'RUB']],
              $item->pay_id,
              $idempotence_key
            );
  
            $item->status = $response->status;
            if ($response->status === "succeeded") {
              $item->approved_at = date("Y-m-d H:i:s");
              if (Auth::user()->team_number == null) {
                Auth::user()->team_number = User::max('team_number') + 1;
                Auth::user()->save();
              }
              Mail::to(Auth::user()->email)->send( new PaymentComplete(Auth::user()) );
              $unique_code = ($payment_succeed) ? $tpl : $unique_code;
            }
            $item->save();
          }
          
          if ($payment->status === "succeeded") {
            $item->status = "succeeded";
            $item->approved_at = date("Y-m-d H:i:s");
            if (Auth::user()->team_number == null) {
              Auth::user()->team_number = User::max('team_number') + 1;
              Auth::user()->save();
            }
            Mail::to(Auth::user()->email)->send( new PaymentComplete(Auth::user()) );
            $unique_code = ($payment_succeed) ? $tpl : $unique_code;

            $item->save();
          }
        }
      }
    }

    return compact('payment_list', 'unique_code');
  }


  /**
   * Обработка платежей
   */
  public function process (Request $request) {
    $input = json_encode($request->all(), true);
		Logger::create([
			'request' => $input
    ]);
    $input = $request->all();
    
    if(isset($input['event'])) {
      $notification = ($input['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
        ? new NotificationSucceeded($input)
        : new NotificationWaitingForCapture($input);
    }
    else {
      return response()->json(['message' => 'event is not provided'], 400);
    }

    $payment = $notification->getObject();
    $item = Payment::where('pay_id', $payment->id)->first();
    $user = $item->user;
    
    $client = new Client();
    $client->setAuth(config('ya_kassa.shop_id'), config('ya_kassa.secret'));

    if ($payment->status === "waiting_for_capture") {
      $item->status = $payment->status;
      $idempotence_key = uniqid('', true).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
      # Списываем платеж
      $response = $client->capturePayment(
        ['amount' => ['value' => $item->amount, 'currency' => 'RUB']],
        $item->pay_id,
        $idempotence_key
      );
    }
    
    if ($payment->status === "succeeded") {
      $item->status = $payment->status;
      $item->approved_at = date("Y-m-d H:i:s");
      $item->save();
      if ($user->team_number == null) {
        $user->team_number = User::max('team_number') + 1;
        $user->save();
      }
      Mail::to($user->email)->send( new PaymentComplete($user) );
    }
    return response()->json(['success' => 'success'], 200);
  }



  /**
	 * Check payments
	 */
	public function check_payments(Request $request)
	{
    if (!$request->exists('key') || $request->get('key') !== 'truebrave')
      dd('Sqrepi Qoqoo');

    $payment_list = Payment::with('user')->get();

    if (count($payment_list) > 0) {
      $client = new Client();
      $client->setAuth(config('ya_kassa.shop_id'), config('ya_kassa.secret'));
      
      echo ('<div><code>Обрабатываем не подтвержденные платежи:</code><hr></div>');
      
      foreach ($payment_list as $key => $item) {
        # Обрабатываем только не подтвержденные платежи
        if ($item->approved_at === null) {

          # Выводим информацию о платеже
          $user = $item->user;
          echo ("<div><code>Платеж id  <b>$item->id</b>, pay_id <b>$item->pay_id</b> от <b>$item->created_at</b>, 
          пользовател <b>$user->email</b>, в статусе <b>$item->status</b></code></div>");

          # Полчаем информацию о платеже из ya kassa
          $payment = $client->getPaymentInfo($item->pay_id);

          # Если статус waiting_for_capture - ждет списания
          if ($payment->status === "waiting_for_capture") {
            $idempotence_key = uniqid('', true).rand(0,9).rand(0,9).rand(0,9).rand(0,9);
            # Списываем платеж
            $response = $client->capturePayment(
              ['amount' => ['value' => $item->amount, 'currency' => 'RUB']],
              $item->pay_id,
              $idempotence_key
            );

            $item->status = $response->status;
            if ($response->status === "succeeded") {
              $item->approved_at = date("Y-m-d H:i:s");
              if ($item->user->team_number == null) {
                $item->user->team_number = User::max('team_number') + 1;
                $item->user->save();
              }
              Mail::to($item->user->email)->send( new PaymentComplete($item->user) );
            }
            $item->save();
          }

          # Если статус succeeded - успешно списан
          if ($payment->status === "succeeded") {
            $item->status = "succeeded";
            $item->approved_at = date("Y-m-d H:i:s");
            if ($item->user->team_number == null) {
              $item->user->team_number = User::max('team_number') + 1;
              $item->user->save();
            }
            Mail::to($item->user->email)->send( new PaymentComplete($item->user) );
            $item->save();
          }
        }
         
      } # foreach
    } # if (count($payment_list) > 0)

  }


  /**
	 * Fix team numbers
	 */
	public function recalculate_team_numbers(Request $request)
	{
    if (!$request->exists('key') || $request->get('key') !== 'truebrave')
      dd('Sqrepi Qoqoo');

    $user_list = User::with('payments')->athletes()->registred()->orderBy('team_number')->get();
    $incorrect = false;
    $number = 1;
    foreach ($user_list as $key => $user) {
      if ($key == 0) {
        if ($user->team_number !== 1) {
          echo ("<div><code>Wrong number! id $user->id - team number $user->team_number</code></div>");
        }
      }
      else {
        if ($user->team_number !== $user_list[$key-1]->team_number+1) {
          $incorrect = true;
          echo ("<div><code>Wrong number! id $user->id - team number $user->team_number. Missing previous teamnumber.</code></div>");
        } 
      }
    }

    # Если есть что корректировать
    if( $incorrect ) {
      echo ("<div><code>Starting correction...</code></div>");
      foreach ($user_list as $key => $user) {
        $user->team_number = $number;
        $user->save();
        $number++;
      }
    }
    $result = ($incorrect) ? 'The sequence of numbers fixed.' : 'The sequence of numbers is correct ^_^.';
    echo ("<div><code>Work done. $result</code></div>");
    return response(null, 200);
  }
}