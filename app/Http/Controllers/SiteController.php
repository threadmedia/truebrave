<?php

namespace App\Http\Controllers;

use Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

use App\Mail\ActivateLinkEmail;
use App\Mail\TeamRegistred;
use App\Mail\PaymentComplete;

use Barryvdh\DomPDF\Facade as PDF;
use YandexCheckout\Client;

use App\User;
use App\Payment;
use App\MailQueue;
use App\Logger;

class SiteController extends Controller
{

	public function index()
	{
		$page_title = 'Стать спонсором';
		$payment_list = Payment::succeeded()->where('amount', '!=', 1)->get();
		$team_list = User::registred()->get();
		return view('site.index', compact('page_title', 'payment_list', 'team_list'));
	}


	/**
	 * Авторизация через токен с активацией
	 */
	public function activate(Request $request)
	{
		if ($request->exists('token')) {
			$user = User::where('auth_token', $request->get('token'))->first();
			if ($user) {
				Auth::guard()->login($user);
				return redirect()->route('site.order');	
			}
		}
		return view('site.not-authorized');
	}
	

	/**
	 * Order selection
	 */
	public function order(Request $request)
	{
		$payment_list = Payment::where('user_id', Auth::id())->get();
		$user = Auth::user();
		return view('site.order', compact('payment_list', 'user'));
	}





	/**
	 * Order selection
	 */
	public function payment_result(Request $request)
	{
		return view('site.payment-result');
	}


	/**
	 * 
	 */
	public function logout(Request $request)
	{
		Auth::logout();
		return redirect()->route('site.index');	
	}


	/**
	 * 
	 */
	public function login(Request $request)
	{
		return redirect()->route('site.index');	
	}



	/**
	 * Тест писем
	 */
	public function mailTest(Request $request)
	{
		if($request->exists('query')) {
			Mail::to(Auth::user()->email)->send( new TeamRegistred(Auth::user()) );
			Mail::to(Auth::user()->email)->send( new PaymentComplete(Auth::user()) );
		}
	}


	/**
	 * Метрика
	 */
	public function metrika(Request $request)
	{
		$territory_list = Territory::with('users')->get();
		return view('site/metrika', compact('territory_list'));
	}


	/**
	 * Dash
	 */
	public function dash(Request $request)
	{
		if($request->exists('hash') && $request->get('hash') == 'diedbubblerule') {
			return view('site/dash', compact('territory_list'));
		}
		else
			return false;
	}


	/**
	 * Метрика
	 */
	public function pdfTest(Request $request)
	{
		$user = User::find(3);
		$data = ['user' => $user];
		PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
		$pdf = PDF::loadView('docs/ticket', $data);
		$pdf->save('tickets/' . $user->id . '-ticket.pdf');
	}


	/**
	 * Logger test
	 */
	public function loggerTest(Request $request)
	{
		$input = json_encode($request->all(), true);
		Logger::create([
			'request' => $input
		]);
	}
}