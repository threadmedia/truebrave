<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',           'SiteController@index')->name('site.index');
Route::get('activate',   'SiteController@activate')->name('site.activate');
Route::get('login',   'SiteController@login')->name('login');

Route::middleware(['auth'])->group( function () {
  Route::get('order',     'SiteController@order')->name('site.order');
  Route::get('logout',    'SiteController@logout')->name('site.logout');
  
  Route::middleware(['CheckAdminRole'])->group(function () {
    Route::prefix('users')->group(function () {
      Route::get('/',    'UserController@index')->name('user.index');
    });
  });
});

Route::get('/mail-test', 'SiteController@mailTest')->name('mail-test');
Route::get('/logger-test', 'SiteController@loggerTest')->name('logger-test');

Route::get('/metrika', ['uses' => 'SiteController@metrika', 'as' => 'metrika']);
Route::get('/dash',    ['uses' => 'SiteController@dash',    'as' => 'dash']);
