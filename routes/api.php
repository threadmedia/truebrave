<?php

use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::namespace('Api')->group(function() {

	Route::prefix('auth')->group(function () {
		Route::post('signup', 					'AuthController@signup');
		Route::post('send-auth-link', 	'AuthController@send_auth_link');
	});

	Route::middleware(['auth'])->group( function () {
		Route::prefix('payment')->group(function () {
			Route::post('pay',		'PaymentController@pay')->name('payment.pay');
			Route::post('check',	'PaymentController@check')->name('payment.check');
		});

		Route::middleware(['CheckAdminRole'])->group(function () {
			Route::prefix('users')->group(function () {
				Route::get('/',    			'UserController@list')->name('user.list');
				Route::post('{id}',			'UserController@confirm')->name('user.confirm')->where('id', '[0-9]+');
				Route::get('/preroll/{id}',			'UserController@preroll')->where('id', '[0-9]+');
				Route::get('/prerollTest',			'UserController@prerollTest');
				Route::get('/mass_preroll',			'UserController@mass_preroll');
				
				Route::put('{id}',    	'UserController@update')->name('user.update')->where('id', '[0-9]+');
				Route::delete('{id}',   'UserController@trash')->name('user.trash')->where('id', '[0-9]+');
				
				Route::get('recycle-bin',			'UserController@recycle_bin')->name('user.recycle-bin');
				Route::delete('/delete/{id}',	'UserController@delete')->name('user.delete');
			});
		});
	});

	Route::prefix('payment')->group(function () {
		Route::any('process',  				'PaymentController@process')->name('payment.process');
		Route::get('check-payments',	'PaymentController@check_payments')->name('payment.check_payments');
		Route::get('recalculate',			'PaymentController@recalculate_team_numbers')->name('payment.recalculate');
	});

	// Route::prefix('dash')->group(function () {
	// 	Route::get('init', 'DashController@init');
	// });

	// Route::prefix('users')->group(function () {
	// 	Route::any('search', 'DashController@search');
	// 	Route::get('clear/{user_id}', 'DashController@clear')->where('user_id', '[0-9]+');
	// });
});